package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonethics.valleygroup.R;

public class MilestoneListAdapter extends ArrayAdapter<String>{

	ArrayList<String> milestones;
	LayoutInflater inflater;
	Typeface mTypeFace;
	int screenNum;

	public MilestoneListAdapter(Activity context,ArrayList<String> milestones, int screenNum) {
		super(context,  R.layout.milestoneview);
		// TODO Auto-generated constructor stub

		this.milestones = milestones;
		this.screenNum = screenNum;
		inflater = context.getLayoutInflater();
		Log.d("SIZE ","SIZE " + milestones.size());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return milestones.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub\\
		
		if(screenNum!=0){
			
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflater.inflate(R.layout.milestoneview, null);
				holder.tv = (TextView) convertView.findViewById(R.id.mileStoneText);
				holder.iv = (ImageView) convertView.findViewById(R.id.mMilestoneImg);
				convertView.setTag(holder);
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.tv.setText(milestones.get(position));
			mTypeFace = Typeface.createFromAsset(convertView.getContext().getAssets(),"fonts/arial.ttf");
			hold.tv.setTypeface(mTypeFace);
		}
		else{
			
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflater.inflate(R.layout.formslist, null);
				holder.tv = (TextView) convertView.findViewById(R.id.mFormsTitle);
				//holder.iv = (ImageView) convertView.findViewById(R.id.mMilestoneImg);
				convertView.setTag(holder);
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.tv.setText(milestones.get(position));
			mTypeFace = Typeface.createFromAsset(convertView.getContext().getAssets(),"fonts/arial.ttf");
			hold.tv.setTypeface(mTypeFace);
		}
		
		
//		if(screenNum == 0){
//			
//			hold.iv.setVisibility(View.GONE);
//		}
//		else{
//			
//			hold.iv.setVisibility(View.VISIBLE);
//		}

		return convertView;
	}

	static class ViewHolder
	{
		TextView tv;
		ImageView iv;
	}

}
