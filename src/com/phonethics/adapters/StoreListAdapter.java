package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonethics.model.StoreDetails;
import com.phonethics.valleygroup.R;

public class StoreListAdapter extends ArrayAdapter<StoreDetails>{

	//ArrayList<String> storeNames = new ArrayList<String>();
	ArrayList<StoreDetails> storeDetails;
	Context context;
	LayoutInflater inflater;
	Typeface mTypeFace;

	public StoreListAdapter(Activity context, ArrayList<StoreDetails> storeDetails) {
		super(context, R.layout.store_custom_layout);
		// TODO Auto-generated constructor stub

		this.context = context;
		this.storeDetails = storeDetails;
		inflater = context.getLayoutInflater();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return storeDetails.size();
	}

	@Override
	public StoreDetails getItem(int position) {
		// TODO Auto-generated method stub
		return storeDetails.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub\\

		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflater.inflate(R.layout.store_custom_layout, null);
			holder.tv = (TextView) convertView.findViewById(R.id.storeName);
			holder.address = (TextView) convertView.findViewById(R.id.storeAddress);
			convertView.setTag(holder);
		}

		ViewHolder hold=(ViewHolder)convertView.getTag();
		mTypeFace = Typeface.createFromAsset(convertView.getContext().getAssets(),"fonts/arial.ttf");
		hold.tv.setTypeface(mTypeFace);
		hold.address.setTypeface(mTypeFace);
		hold.tv.setText(getItem(position).getStore_name());
		hold.address.setText(getItem(position).getStore_address());
		
		//set store status icon
		if(getItem(position).getStatus().equalsIgnoreCase("1")){
			
			hold.tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.status_icon_installation_new, 0);
		}
		else if(getItem(position).getStatus().equalsIgnoreCase("2")){
			
			hold.tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.status_icon_transit_new, 0);
		}
		else if(getItem(position).getStatus().equalsIgnoreCase("3")){
			
			hold.tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.status_icon_completed_new, 0);
		}
		else if(getItem(position).getStatus().equalsIgnoreCase("0")){
			
			hold.tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.status_icon_notstarted, 0);
		}
		

		return convertView;
	}

	class ViewHolder
	{
		TextView tv;
		TextView address;
		ImageView iv;
	}

}



