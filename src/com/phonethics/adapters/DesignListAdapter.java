package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonethics.model.DesignDetailModel;
import com.phonethics.valleygroup.R;
import com.squareup.picasso.Picasso;

public class DesignListAdapter extends ArrayAdapter<DesignDetailModel> {

	ArrayList<DesignDetailModel> designs;
	Context mContext;
	LayoutInflater inflater;
	Typeface mTypeFace;
	String PARENT_URL;

	public DesignListAdapter(Activity context,ArrayList<DesignDetailModel> designs) {
		super(context,  R.layout.designlistcustomlayout);
		// TODO Auto-generated constructor stub

		mContext = context;
		this.designs = designs;
		inflater = context.getLayoutInflater();

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return designs.size();
	}

	@Override
	public DesignDetailModel getItem(int position) {
		// TODO Auto-generated method stub
		return designs.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub\\

		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflater.inflate(R.layout.designlistcustomlayout, null);
			holder.txtView = (TextView) convertView.findViewById(R.id.mDesignText);
			holder.imgView = (ImageView) convertView.findViewById(R.id.mDesignImage);
			convertView.setTag(holder);
		}

		ViewHolder hold=(ViewHolder)convertView.getTag();
		hold.txtView.setText(getItem(position).getName());
		mTypeFace = Typeface.createFromAsset(convertView.getContext().getAssets(),"fonts/arial.ttf");
		hold.txtView.setTypeface(mTypeFace,Typeface.BOLD);

		PARENT_URL = mContext.getResources().getString(R.string.base_url);
		Picasso.with(mContext)
		.load(PARENT_URL+ getItem(position).getThumb_url())
		.placeholder(R.drawable.ic_launcher)
		.error(R.drawable.ic_launcher)
		.into(hold.imgView);


		return convertView;
	}

	class ViewHolder
	{
		TextView txtView;
		ImageView imgView;
	}


}
