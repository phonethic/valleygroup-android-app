package com.phonethics.adapters;

import java.util.ArrayList;

public class AnswerModel {

	private String question_id;
	private String question_type;
	private ArrayList<String> answers = new ArrayList<String>();
	
	
	public String getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(String question_id) {
		this.question_id = question_id;
	}
	public String getQuestion_type() {
		return question_type;
	}
	public void setQuestion_type(String question_type) {
		this.question_type = question_type;
	}
	public ArrayList<String> getAnswers() {
		return answers;
	}
	public void setAnswers(String answers) {
		this.answers.add(answers);
	}
	
	

}
