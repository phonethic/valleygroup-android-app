package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.phonethics.valleygroup.R;
import com.squareup.picasso.Picasso;

public class StoreGalleryAdapter extends ArrayAdapter<String>{

	ArrayList<String> thumbImageUrls;
	LayoutInflater inflater;
	Context mContext;

	public StoreGalleryAdapter(Activity context, int resource, ArrayList<String> thumbImageUrls) {
		super(context, resource);
		// TODO Auto-generated constructor stub

		this.thumbImageUrls = thumbImageUrls;
		inflater = context.getLayoutInflater();
		mContext = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return thumbImageUrls.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflater.inflate(R.layout.storecompletegalleryview, null);
			holder.tv = (ImageView) convertView.findViewById(R.id.mGalleryImage);
			convertView.setTag(holder);
		}

		ViewHolder hold=(ViewHolder)convertView.getTag();
		Picasso.with(mContext)
		.load(thumbImageUrls.get(position))
		.placeholder(R.drawable.ic_launcher)
		.error(R.drawable.ic_launcher)
		.into(hold.tv);


		return convertView;
	}

	static class ViewHolder
	{

		ImageView tv;
	}

}
