package com.phonethics.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

public class InstallationQuestionModel {

	private String category_id;
	private String store_id;
	private String question;
	private String question_type;
	private String question_id;
	private String option_type;

	private ArrayList<String> answers= new ArrayList<String>();

	private ArrayList<String> options = new ArrayList<String>();

	private Map<String,String> mMap = new HashMap<String,String>();

	private ArrayList<Map<String,String>> mArrMap=new ArrayList<Map<String,String>>();
	
	private String txtBoxMessage;
	
	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String story_id) {
		this.store_id = story_id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestion_type() {
		return question_type;
	}

	public void setQuestion_type(String question_type) {
		this.question_type = question_type;
	}

	public String getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(String question_id) {
		this.question_id = question_id;
	}

	public String getOption_type() {
		return option_type;
	}

	public void setOption_type(String option_type) {
		this.option_type = option_type;
	}

	public ArrayList<String> getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options.add(options);
	}

	public ArrayList<String> getAnswers() {
		return answers;
	}
	public void setAnswers(String answers) {

		//Log.d("ANSWERS:","ANSWERS: " + answers);
		this.answers.add(answers);
	}

	public Map<String,String> getmMap() {
		Log.d("HASHMAP getmMap:","HASHMAP - getmMap: "+ mMap.toString());
		return mMap;
	}

	public void setmMap(String key, String value) {
		this.mMap.put(key, value);
		Log.d("HASHMAP OBJ:","HASHMAP - setMap: "+ mMap.toString());
	}

	public void addMap(){
		mArrMap.add(mMap);
		Log.d("MAPOBJ","MAPOBJ" + mMap.toString());
	}

	public Map<String,String> getMap(int position){
		
		return mArrMap.get(position);

	}
	
	public void setMap(String key, String value, int position){
		
		mArrMap.get(position).put(key, value);
	}

	public String getTxtBoxMessage() {
		return txtBoxMessage;
	}

	public void setTxtBoxMessage(String txtBoxMessage) {
		this.txtBoxMessage = txtBoxMessage;
	}

	
}
