package com.phonethics.model;

public class DesignDetailModel {

	private String thumb_url;
	private String 	store_id;
	private String design_id;
	private String image_url;
	private String name;


	public String getThumb_url() {
		return thumb_url;
	}
	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public String getStore_id() {
		return store_id;
	}
	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public String getDesign_id() {
		return design_id;
	}
	public void setDesign_id(String design_id) {
		this.design_id = design_id;
	}


	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
