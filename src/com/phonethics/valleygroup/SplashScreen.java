package com.phonethics.valleygroup;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SplashScreen extends FragmentActivity {

	//ViewFlipper mViewFlipper;
	private float lastX;
	TextView mSkip;
	ViewPager mViewPager;
	Context mContext;
	ArrayList<Integer> mImgsDrawable = new ArrayList<Integer>();
	LinearLayout mNavigationDots;
	TextView[] mPagerStripCircle;
	SessionManager mSessionManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		mContext = this;
		//mViewFlipper = (ViewFlipper) findViewById(R.id.mViewFlipper);


		mViewPager = (ViewPager) findViewById(R.id.mViewPager);
		mSkip = (TextView) findViewById(R.id.mSkip);
		mNavigationDots = (LinearLayout) findViewById(R.id.mNavigationDots); 
		mSessionManager = new SessionManager(mContext);

		mImgsDrawable.add(R.drawable.valley_info1);
		mImgsDrawable.add(R.drawable.valley_info2);
		mImgsDrawable.add(R.drawable.valley_info3);
		mImgsDrawable.add(R.drawable.valley_info4);
		mImgsDrawable.add(R.drawable.valley_info5);

		mPagerStripCircle = new TextView[mImgsDrawable.size()];

		mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),mContext, mImgsDrawable));
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

				//Toast.makeText(mContext, "index " + arg0, 0).show();


				for(int i=0;i<mPagerStripCircle.length;i++){

					if(i==arg0){

						mPagerStripCircle[i].setTextColor(Color.parseColor("#FFFFFF"));	
					}
					else{

						mPagerStripCircle[i].setTextColor(Color.parseColor("#000000"));
					}
				}

				if(arg0 == mImgsDrawable.size()-1){

					mSkip.setText("Enter");
					//mPagerStripCircle[arg0].setTextColor(Color.parseColor("#FFFFFF"));	
				}
				else{

					mSkip.setText("Skip");
					//mPagerStripCircle[arg0].setTextColor(Color.parseColor("#000000"));
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub


			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});


		mSkip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(!mSessionManager.isLoggedInCustomer() && !mSessionManager.isLoggedInFrontlineMember()){

					Intent intent = new Intent(mContext, LoginGateway.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				else if(mSessionManager.isLoggedInCustomer()){


					Intent intent = new Intent(mContext, StoreList.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else if(mSessionManager.isLoggedInFrontlineMember()){


					Intent intent = new Intent(mContext, MemberModuleSelection.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});

		// for pagerstrip
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((LayoutParams.WRAP_CONTENT), (LayoutParams.WRAP_CONTENT));

		for(int i=0;i<mPagerStripCircle.length;i++){

			mPagerStripCircle[i] = new TextView(mContext);
			mPagerStripCircle[i].setLayoutParams(lp);
			mPagerStripCircle[i].setTextSize(40);
			mPagerStripCircle[i].setText(".");
			mPagerStripCircle[i].setGravity(Gravity.CENTER);

			mNavigationDots.addView(mPagerStripCircle[i]);
			mPagerStripCircle[i].setTextColor(Color.parseColor("#000000"));

		}

		mPagerStripCircle[0].setTextColor(Color.parseColor("#FFFFFF"));
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}

	class ViewPagerAdapter extends FragmentPagerAdapter{

		Context context;
		ArrayList<Integer> images = null;

		public ViewPagerAdapter(FragmentManager fm, Context context, ArrayList<Integer> images) {
			super(fm);
			// TODO Auto-generated constructor stub

			this.context = context;
			this.images = images;
		}

		@Override
		public Fragment getItem(int arg0) {
			// TODO Auto-generated method stub


			return SplashPagerFragment.newInstance(images.get(arg0));

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return images.size();
		}


	}


	public static class SplashPagerFragment extends Fragment {

		Context context;
		Integer images;


		public SplashPagerFragment()
		{

		}

		public static SplashPagerFragment newInstance(Integer images) {
			SplashPagerFragment fragmentFirst = new SplashPagerFragment();
			Bundle args = new Bundle();
			args.putInt("images", images);
			fragmentFirst.setArguments(args);
			return fragmentFirst;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);

			images = getArguments().getInt("images");

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub

			View view = inflater.inflate(R.layout.splashimageholder, container, false);
			ImageView imgView = (ImageView) view.findViewById(R.id.ivBigImagePager);
			imgView.setImageResource(images);

			imgView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub


				}
			});

			return view;
		}

		@Override
		public void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
		}



	}

}
