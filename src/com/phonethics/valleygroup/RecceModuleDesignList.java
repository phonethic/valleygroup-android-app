package com.phonethics.valleygroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phonethics.adapters.DesignListAdapter;
import com.phonethics.model.DesignDetailModel;

public class RecceModuleDesignList extends ActionBarActivity {

	Context mContext;
	ListView mRecceDesignList;
	SessionManager session;
	ProgressBar mProgressBarDesignList;
	String STORE_ID;
	String DESIGN_URL;
	String mMember_ID;
	String mMember_Auth_id;
	HashMap<String, String> mMemberDetails = new HashMap<String, String>();
	String mSuccess; 
	DesignDetailModel mDesignDetails;
	ArrayList<DesignDetailModel> designDetails = new ArrayList<DesignDetailModel>();
	DesignListAdapter mAdapter;
	Activity mAContext;
	ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recce_module_design_list);

		mContext = this;
		mAContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Choose Design");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
		mRecceDesignList = (ListView) findViewById(R.id.mRecceDesignList);
		mProgressBarDesignList = (ProgressBar) findViewById(R.id.mProgressBarDesignList);

		Bundle bundle=getIntent().getExtras();

		if(bundle!=null){

			STORE_ID = bundle.getString("STORE_ID");
		}

		DESIGN_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.recce_design);

		DESIGN_URL = DESIGN_URL + "?store_id=" + STORE_ID;

		mMemberDetails = session.getMemberDetails();
		mMember_ID = mMemberDetails.get(SessionManager.MEMBER_ID);
		mMember_Auth_id = mMemberDetails.get(SessionManager.MEMBER_AUTH_ID);

		callDesignListApi();

		mRecceDesignList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(mContext, RecceModuleChooseLocation.class);
				intent.putExtra("store_id", designDetails.get(position).getStore_id());
				intent.putExtra("design_id",designDetails.get(position).getDesign_id());
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
	}

	private void callDesignListApi() {
		// TODO Auto-generated method stub

		mProgressBarDesignList.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("member_id", mMember_ID);
		headers.put("auth_id", mMember_Auth_id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, DESIGN_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mProgressBarDesignList.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseResponse(api_response);
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mProgressBarDesignList.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);
	}

	void parseResponse(String api_response) {
		// TODO Auto-generated method stub


		try {

			JSONObject json = new JSONObject(api_response);
			mSuccess = json.getString("success");

			if(mSuccess.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");

				for(int i=0;i<data.length();i++){

					mDesignDetails = new DesignDetailModel();

					JSONObject designDetailsJson = data.getJSONObject(i);

					mDesignDetails.setThumb_url(designDetailsJson.getString("thumb_url"));
					mDesignDetails.setStore_id(designDetailsJson.getString("store_id"));
					mDesignDetails.setDesign_id(designDetailsJson.getString("design_id"));
					mDesignDetails.setImage_url(designDetailsJson.getString("image_url"));
					mDesignDetails.setName(designDetailsJson.getString("name"));

					designDetails.add(mDesignDetails);
				}

				mAdapter = new DesignListAdapter(mAContext, designDetails);
				mRecceDesignList.setAdapter(mAdapter);
			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");


				if(errorCode.equalsIgnoreCase("-116")){

					showAlertDialog();
				}
				else{
					showToast(errorMessage);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					//logout customer
					session.logoutMember();
					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();

					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}
		return true;
	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutMember();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	void showToast(String msg){

		Toast.makeText(mContext, ""+ msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}
}
