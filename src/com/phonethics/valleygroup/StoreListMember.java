package com.phonethics.valleygroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phonethics.adapters.StoreListAdapter;
import com.phonethics.model.StoreDetails;

public class StoreListMember extends ActionBarActivity {

	Context mContext;
	EditText mSearchBoxMember;
	ListView mStoreListMember;
	ProgressBar mProgressBarStoreListMember;
	String ISTALLATION_STORE_URL;
	String RECCE_STORE_URL;
	SessionManager session;
	String mMember_ID;
	String mMember_Auth_id;
	HashMap<String, String> mMemberDetails = new HashMap<String, String>();
	String mModuleType;
	String mSuccess;
	StoreDetails mStoreDetailModel;
	ArrayList<StoreDetails> storeDetailsData = new ArrayList<StoreDetails>();
	StoreListAdapter adapter;
	Activity mAContext;
	String STORE_ID;
	int textlength =  0;
	ActionBar mActionBar;
	TextView mSearchedText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_list_member);

		mContext = this;
		mAContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));

		Bundle bundle=getIntent().getExtras();
		if(bundle!=null){

			mModuleType = bundle.getString("module_type");
		}

		mSearchBoxMember = (EditText) findViewById(R.id.mSearchBoxMember);
		mStoreListMember = (ListView) findViewById(R.id.mStoreListMember);
		mProgressBarStoreListMember = (ProgressBar) findViewById(R.id.mProgressBarStoreListMember);
		mSearchedText = (TextView) findViewById(R.id.mSearchedTextMemeber);

		//Store list url for installation module
		ISTALLATION_STORE_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.stores) + "?install_module=1";

		//Store List url for Recce module
		RECCE_STORE_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.stores);

		mMemberDetails = session.getMemberDetails();
		mMember_ID = mMemberDetails.get(SessionManager.MEMBER_ID);
		mMember_Auth_id = mMemberDetails.get(SessionManager.MEMBER_AUTH_ID);

		//		Log.d("AUTH_ID","AUTH_ID " +  mMember_Auth_id);
		//		Log.d("Member_ID","Member_ID " +  mMember_ID);
		//		Log.d("RES","RES " +  ISTALLATION_STORE_URL);

		if(mModuleType.equalsIgnoreCase("Installation")){

			callInstallationStoreListApi();
		}
		else if(mModuleType.equalsIgnoreCase("Recce")){

			callRecceStoreListApi();
		}

		mStoreListMember.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				//showToast(storeDetailsData.get(position).getStore_id());

				STORE_ID = storeDetailsData.get(position).getStore_id();

				if(mModuleType.equalsIgnoreCase("Recce")){

					Intent intent = new Intent(mContext, RecceModuleDesignList.class);
					intent.putExtra("STORE_ID", STORE_ID);
					startActivity(intent);	
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else{

					Intent intent = new Intent(mContext, InstallationFormCategory.class);
					intent.putExtra("STORE_ID", STORE_ID);
					startActivity(intent);	
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});

		mSearchBoxMember.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				mSearchBoxMember.setBackgroundResource(R.drawable.textboxback_black);
				mSearchBoxMember.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.close_search_icon_new, 0);
				mSearchedText.setVisibility(View.VISIBLE);

				textlength = mSearchBoxMember.getText().length();
				String text = mSearchBoxMember.getText().toString();

				ArrayList<StoreDetails> STORE_INFOS_TEMP = new ArrayList<StoreDetails>();
				for (int i = 0; i < storeDetailsData.size(); i++){
					StoreDetails storeInfo  = storeDetailsData.get(i);
					if(textlength <= storeInfo.getStore_name().length()){

						if (text.equalsIgnoreCase((String) storeInfo.getStore_name().subSequence(0, textlength))){

							STORE_INFOS_TEMP.add(storeInfo); 

						}
					}
				}

				if(textlength == 0){

					mSearchBoxMember.setBackgroundResource(R.drawable.textboxback);
					mSearchBoxMember.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.search_icon_new, 0);
					mSearchedText.setVisibility(View.GONE);
				}

				adapter = new StoreListAdapter(mAContext,STORE_INFOS_TEMP);
				mStoreListMember.setAdapter(adapter);

				onItemClickListner(STORE_INFOS_TEMP);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}


	void onItemClickListner(final ArrayList<StoreDetails> newStoreListMerchant) {
		// TODO Auto-generated method stub

		mStoreListMember.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				//showToast(storeDetailsData.get(position).getStore_id());

				STORE_ID = newStoreListMerchant.get(position).getStore_id();

				if(mModuleType.equalsIgnoreCase("Recce")){

					Intent intent = new Intent(mContext, RecceModuleDesignList.class);
					intent.putExtra("STORE_ID", STORE_ID);
					startActivity(intent);	
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else{

					Intent intent = new Intent(mContext, InstallationFormCategory.class);
					intent.putExtra("STORE_ID", STORE_ID);
					startActivity(intent);	
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});
	}


	void callInstallationStoreListApi() {
		// TODO Auto-generated method stub

		mProgressBarStoreListMember.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("member_id", mMember_ID);
		headers.put("auth_id", mMember_Auth_id);

		Log.d("AUTH_ID","AUTH_ID " +  mMember_Auth_id);
		Log.d("Member_ID","Member_ID " +  mMember_ID);
		Log.d("RES","RES " +  ISTALLATION_STORE_URL);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, ISTALLATION_STORE_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mProgressBarStoreListMember.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseInstallationResponse(api_response);
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mProgressBarStoreListMember.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}

	protected void parseInstallationResponse(String api_response) {
		// TODO Auto-generated method stub


		try {

			JSONObject json = new JSONObject(api_response);
			mSuccess = json.getString("success");

			if(mSuccess.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");

				for(int i=0;i<data.length();i++){

					mStoreDetailModel = new StoreDetails();

					JSONObject storeDetails = data.getJSONObject(i);
					//mStoreNames.add(storeDetails.getString("store_name"));

					//to set the store details in Respective POJO
					mStoreDetailModel.setStore_id(storeDetails.getString("store_id"));
					mStoreDetailModel.setCity_name(storeDetails.getString("city_name"));
					mStoreDetailModel.setProject_name(storeDetails.getString("project_name"));
					mStoreDetailModel.setStatus(storeDetails.getString("status"));
					mStoreDetailModel.setStore_name(storeDetails.getString("store_name"));
					mStoreDetailModel.setProject_id(storeDetails.getString("project_id"));
					mStoreDetailModel.setStore_address(storeDetails.getString("store_address"));
					mStoreDetailModel.setStore_description(storeDetails.getString("store_description"));
					mStoreDetailModel.setStore_pincode(storeDetails.getString("store_pincode"));

					storeDetailsData.add(mStoreDetailModel);
				}

				adapter = new StoreListAdapter(mAContext,storeDetailsData);
				mStoreListMember.setAdapter(adapter);

			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");


				if(errorCode.equalsIgnoreCase("-116")){

					showAlertDialog();
				}
				else{

					showToast(errorMessage);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	private void callRecceStoreListApi() {
		// TODO Auto-generated method stub

		mProgressBarStoreListMember.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("member_id", mMember_ID);
		headers.put("auth_id", mMember_Auth_id);



		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, RECCE_STORE_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mProgressBarStoreListMember.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseInstallationResponse(api_response);
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mProgressBarStoreListMember.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);
	}


	void showToast(String msg){

		Toast.makeText(mContext, ""+ msg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					//logout customer
					session.logoutMember();
					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();


		}
		return true;
	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutMember();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}
}
