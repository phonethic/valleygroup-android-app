package com.phonethics.valleygroup;

import java.util.HashMap;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class StoreStatus extends ActionBarActivity {

	Context mContext;
	TextView mStatusText;
	SessionManager session;
	Typeface mTypeFace;
	Typeface mPageTitle;
	Button mStatusButton;
	Animation mRotation;
	ImageView mStatusImagGlowRing;
	String STORE_ID;
	String STORE_STATUS;
	ActionBar mActionBar;
	ImageView mStatusImage;
	String STORE_NAME;
	String Store_Adress;
	TextView mDisplayStoreAdd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_status);

		mContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Store Status");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));

		mStatusButton = (Button) findViewById(R.id.mStatusButton);
		mStatusText = (TextView) findViewById(R.id.mStatusText);
		mStatusImagGlowRing = (ImageView) findViewById(R.id.mStatusImagGlowRing);
		mStatusImage = (ImageView) findViewById(R.id.mStatusImage);
		mDisplayStoreAdd = (TextView) findViewById(R.id.mStoreAddress);

		mTypeFace = Typeface.createFromAsset(getAssets(),"fonts/Arial_Bold.ttf");
		mPageTitle =  Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");

		mStatusText.setTypeface(mPageTitle);
		mStatusButton.setTypeface(mTypeFace);
		mDisplayStoreAdd.setTypeface(mPageTitle);

		Bundle bundle=getIntent().getExtras();

		if(bundle!=null){

			STORE_ID = bundle.getString("STORE_ID");
			STORE_STATUS = bundle.getString("STORE_STATUS");
			STORE_NAME = bundle.getString("STORE_NAME");
			Store_Adress = bundle.getString("STORE_ADDRESS");
		}

		mStatusText.setText(STORE_NAME);
		mDisplayStoreAdd.setText(Store_Adress);

		//rotation code
		mRotation = AnimationUtils.loadAnimation(StoreStatus.this, R.anim.rotate_anim);
		mStatusImagGlowRing.startAnimation(mRotation);


		// change UI according to status
		if(STORE_STATUS.equalsIgnoreCase("1")){

			mStatusButton.setText("View It LIVE");
			mStatusImage.setImageResource(R.drawable.status_in_progress);

		}
		else if(STORE_STATUS.equalsIgnoreCase("2")){

			mStatusButton.setText("View GPS Location");
			mStatusImage.setImageResource(R.drawable.status_in_transit);

		}
		else if(STORE_STATUS.equalsIgnoreCase("3")){

			mStatusButton.setText("View Images");
			mStatusImage.setImageResource(R.drawable.status_completed);
			mStatusImagGlowRing.setVisibility(View.GONE);

		}

		mStatusButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(STORE_STATUS.equalsIgnoreCase("1")){
					
					HashMap<String, String> param = new HashMap<String, String>();
					param.put("StoreName", STORE_NAME);
					param.put("StoreAdd", Store_Adress);
					EventTracker.logEvent("Customer_View_Live_Btn_Pressed", param);

					Intent intent = new Intent(mContext, LiveStreaming.class);
					intent.putExtra("STORE_ID",STORE_ID);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else if(STORE_STATUS.equalsIgnoreCase("2")){
					
					HashMap<String, String> param = new HashMap<String, String>();
					param.put("StoreName", STORE_NAME);
					param.put("StoreAdd", Store_Adress);
					EventTracker.logEvent("Customer_GPS_Location_Btn_Pressed", param);

					Intent intent = new Intent(mContext, InTransitState.class);
					intent.putExtra("STORE_ID",STORE_ID);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else if(STORE_STATUS.equalsIgnoreCase("3")){

					HashMap<String, String> param = new HashMap<String, String>();
					param.put("StoreName", STORE_NAME);
					param.put("StoreAdd", Store_Adress);
					EventTracker.logEvent("Customer_View_Images_Btn_Pressed", param);

					Intent intent = new Intent(mContext, StatusCompleteGalleryView.class);
					intent.putExtra("STORE_ID",STORE_ID);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			//logout customer
			//			session.logoutCustomer();
			//
			//			Intent intent = new Intent(mContext, LoginGateway.class);
			//			ComponentName cn = intent.getComponent();
			//			Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
			//			mContext.startActivity(mainIntent);
			//			finish();
			//			overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);


			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					EventTracker.logEvent("Customer_Logout_Btn_Pressed", false);
					session.logoutCustomer();
					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);


				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}
		return true;
	}


	void showToast(String msg){

		Toast.makeText(mContext, ""+ msg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}
}
