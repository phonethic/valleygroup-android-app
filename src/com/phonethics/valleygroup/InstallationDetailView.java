package com.phonethics.valleygroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phonethics.adapters.MilestoneListAdapter;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class InstallationDetailView extends ActionBarActivity {

	Context mContext;
	String mCustomer_ID;
	String mAuth_Id;
	SessionManager session;
	ProgressBar mMoreOptionProgressBar;
	Activity mAContext;
	MilestoneListAdapter mAdapter;
	ListView mMilestoneList;
	ActionBar mActionBar;
	Typeface mTypeface;
	String STORE_MILESTONE_URL;
	HashMap<String, String> mCustomerDetails = new HashMap<String, String>();
	String STORE_ID;
	ArrayList<String> mStoreMilestone = new ArrayList<String>();
	String success;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_installation_detail_view);

		mContext = this;
		mAContext = this;
		session = new SessionManager(mContext);

		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Installation Details");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
		mMoreOptionProgressBar = (ProgressBar) findViewById(R.id.mMoreOptionProgressBar);
		mTypeface = Typeface.createFromAsset(getAssets(),"fonts/arial.ttf");
		mMilestoneList = (ListView) findViewById(R.id.mMilestoneList);

		//STORE MILESTONE
		STORE_MILESTONE_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.store_milestone);

		mCustomerDetails = session.getUserDetails();
		mCustomer_ID = mCustomerDetails.get(SessionManager.CUSTOMER_ID);
		mAuth_Id = mCustomerDetails.get(SessionManager.AUTH_ID);

		Bundle bundle=getIntent().getExtras();


		if(bundle!=null){

			STORE_ID = bundle.getString("STORE_ID");
		}


		callMilestoneApi();


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			//logout customer
			//			session.logoutCustomer();
			//
			//			Intent intent = new Intent(mContext, LoginGateway.class);
			//			ComponentName cn = intent.getComponent();
			//			Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
			//			mContext.startActivity(mainIntent);
			//			finish();
			//			overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					EventTracker.logEvent("Customer_Logout_Btn_Pressed", false);
					session.logoutCustomer();

					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}
		return true;
	}

	protected void callMilestoneApi() {
		// TODO Auto-generated method stub

		mMoreOptionProgressBar.setVisibility(View.VISIBLE);

		STORE_MILESTONE_URL = STORE_MILESTONE_URL + "?store_id=" +STORE_ID;

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("customer_id", mCustomer_ID);
		headers.put("auth_id", mAuth_Id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, STORE_MILESTONE_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mMoreOptionProgressBar.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseResponse(api_response);
			}




		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mMoreOptionProgressBar.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}

	void showToast(String message) {
		// TODO Auto-generated method stub

		Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
	}

	protected void parseResponse(String api_response) {
		// TODO Auto-generated method stub

		JSONObject json;
		try {

			json = new JSONObject(api_response);
			success = json.getString("success");

			if(success.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");
				for(int i=0;i<data.length();i++){

					JSONObject milestoneDetails = data.getJSONObject(i);
					mStoreMilestone.add(milestoneDetails.getString("milestone"));
				}

				for(int j=0; j<mStoreMilestone.size();j++){

					Log.d("Milestones","Milestones " + mStoreMilestone.get(j));
				}

				mAdapter = new MilestoneListAdapter(mAContext, mStoreMilestone,1);
				mMilestoneList.setAdapter(mAdapter);
			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");
				
				//showToast(errorMessage);

				if(errorCode.equalsIgnoreCase(getResources().getString(R.string.invalid_auth_code))){

					showAlertDialog();
				}
				else{
					
					showToast(errorMessage);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutCustomer();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}
}
