package com.phonethics.valleygroup;

import java.util.Map;

import android.content.Context;

import com.flurry.android.FlurryAgent;
import com.localytics.android.LocalyticsSession;



public class EventTracker { 

	static LocalyticsSession localyticsSession;

	public static void startLocalyticsSession(Context context){

		String keyL = context.getResources().getString(R.string.localactics_id);

		localyticsSession = new LocalyticsSession(context, keyL);
		localyticsSession.open();
		localyticsSession.upload();


	}

	public static void endLocalyticsSession(Context context){
		localyticsSession.close();
		localyticsSession.upload();
	}

	public static void logEvent(String event, boolean isScreen){
		FlurryAgent.logEvent(event);

		if(isScreen) {
			localyticsSession.tagScreen(event);
		}
		else { 
			localyticsSession.tagEvent(event);
		}
	}

	public static void logEvent(String event, Map<String, String> param){
		
		
		FlurryAgent.logEvent(event, param);

		localyticsSession.tagEvent(event, param);


	}

	public static void startFlurrySession(Context context){
		String keyF = context.getResources().getString(R.string.flurryKey);
		FlurryAgent.onStartSession(context, keyF);
	}

	public static void endFlurrySession(Context context){
		FlurryAgent.onEndSession(context);
	}

	public static void reportException(String errorId, String message, String errorClass){
		try
		{
			FlurryAgent.onError(errorId, message, errorClass);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	
}
