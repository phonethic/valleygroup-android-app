package com.phonethics.valleygroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phonethics.adapters.StoreListAdapter;
import com.phonethics.model.StoreDetails;

public class StoreList extends ActionBarActivity {

	ListView mStoreList;
	Context mContext;
	EditText mSearchBox;
	ArrayList<String> mStoreNames = new ArrayList<String>();
	StoreListAdapter adapter;
	Activity mAContext;
	SessionManager session;
	String STORE_URL;

	HashMap<String, String> mCustomerDetails = new HashMap<String, String>();
	String mCustomer_ID;
	String mAuth_Id;
	ProgressBar mLoginProgressBarStoreList;
	String mSuccess;
	StoreDetails mStoreDetailModel = new StoreDetails();
	ArrayList<StoreDetails> storeDetailsData;
	int textlength =  0;
	TextView storeText;
	Typeface mTypeFace;
	Typeface mPageTitle;
	ActionBar mActionBar;
	TextView mSearchedText;
	private long back_pressed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_list);

		mContext = this;
		mAContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Store List");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));


		mTypeFace = Typeface.createFromAsset(getAssets(),"fonts/ABALC.ttf");
		mPageTitle =  Typeface.createFromAsset(getAssets(),"fonts/kalinga.ttf");

		storeDetailsData = new ArrayList<StoreDetails>();
		mStoreList = (ListView) findViewById(R.id.mStoreList);
		mSearchBox = (EditText) findViewById(R.id.mSearchBox);
		mLoginProgressBarStoreList = (ProgressBar) findViewById(R.id.mLoginProgressBarStoreList);
		mSearchedText = (TextView) findViewById(R.id.mSearchedText);
		//storeText = (TextView) findViewById(R.id.storeText);

		//stores url
		STORE_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.stores);


		mCustomerDetails = session.getUserDetails();
		mCustomer_ID = mCustomerDetails.get(SessionManager.CUSTOMER_ID);
		mAuth_Id = mCustomerDetails.get(SessionManager.AUTH_ID);

		Log.d("URL ","URL " + STORE_URL);
		//showToast("HI " + mAuth_Id);

		//storeText.setTypeface(mPageTitle);

		callStoreListApi();

		//		mStoreNames.add("Store 1");
		//		mStoreNames.add("Store 2");
		//		mStoreNames.add("Store 3");

		adapter = new StoreListAdapter(mAContext,storeDetailsData);
		mStoreList.setAdapter(adapter);

		mStoreList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				//callStatusApi(position);
				String status = storeDetailsData.get(position).getStatus();
				String Store_Id = storeDetailsData.get(position).getStore_id();
				String store_name = storeDetailsData.get(position).getStore_name();
				String Store_Address = storeDetailsData.get(position).getStore_address();

				//				if(status.equalsIgnoreCase("1")){
				//
				//					Intent intent = new Intent(mContext, LiveStreaming.class);
				//					intent.putExtra("STORE_ID",Store_Id);
				//					startActivity(intent);
				//				}
				//				else if(status.equalsIgnoreCase("2")){
				//
				//					Intent intent = new Intent(mContext, InTransitState.class);
				//					intent.putExtra("STORE_ID",Store_Id);
				//					startActivity(intent);
				//				}
				//				else if(status.equalsIgnoreCase("3")){
				//
				//					Intent intent = new Intent(mContext, StatusCompleteGalleryView.class);
				//					intent.putExtra("STORE_ID",Store_Id);
				//					startActivity(intent);
				//				}



				if(status.equalsIgnoreCase("0")){

					HashMap<String, String> param = new HashMap<String, String>();
					param.put("StoreName", store_name);
					param.put("StoreAdd", Store_Address);
					EventTracker.logEvent("Customer_Selected_Store_Installation_Details", param);

					Intent intent = new Intent(mContext, InstallationDetailView.class);
					intent.putExtra("STORE_ID", Store_Id);
					startActivity(intent);
				}
				else{

					HashMap<String, String> param = new HashMap<String, String>();
					param.put("StoreName", store_name);
					param.put("StoreAdd", Store_Address);
					EventTracker.logEvent("Customer_Selected_Store", param);

					Intent intent = new Intent(mContext, StoreStatus.class);
					intent.putExtra("STORE_ID",Store_Id);
					intent.putExtra("STORE_STATUS",status);
					intent.putExtra("STORE_NAME",store_name);
					intent.putExtra("STORE_ADDRESS", Store_Address);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}

			}


		});

		mSearchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				EventTracker.logEvent("Customer_Search_Store", false);

				mSearchBox.setBackgroundResource(R.drawable.textboxback_black);
				mSearchBox.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.close_search_icon_new, 0);
				mSearchedText.setVisibility(View.VISIBLE);

				textlength = mSearchBox.getText().length();
				String text = mSearchBox.getText().toString();

				ArrayList<StoreDetails> STORE_INFOS_TEMP = new ArrayList<StoreDetails>();
				for (int i = 0; i < storeDetailsData.size(); i++){
					StoreDetails storeInfo  = storeDetailsData.get(i);
					if(textlength <= storeInfo.getStore_name().length()){

						if (text.equalsIgnoreCase((String) storeInfo.getStore_name().subSequence(0, textlength))){

							STORE_INFOS_TEMP.add(storeInfo); 
							Log.d("TEXT ","TEXT TITLE " + mSearchBox.getText().toString());

						}
						//						else if(storeInfo.getStore_address().contains(text)){
						//
						//							STORE_INFOS_TEMP.add(storeInfo); 
						//							Log.d("TEXT ","TEXT ADD IF " + mSearchBox.getText().toString());
						//						}
					}
					//					else if(storeInfo.getStore_address().contains(text)){
					//
					//						STORE_INFOS_TEMP.add(storeInfo); 
					//						Log.d("TEXT ","TEXT ADD ELSE " + mSearchBox.getText().toString());
					//					}
				}

				if(textlength == 0){

					mSearchBox.setBackgroundResource(R.drawable.textboxback);
					mSearchBox.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.search_icon_new, 0);
					mSearchedText.setVisibility(View.GONE);
				}

				adapter = new StoreListAdapter(mAContext,STORE_INFOS_TEMP);
				mStoreList.setAdapter(adapter);

				onItemClickListner(STORE_INFOS_TEMP);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}


	protected void onItemClickListner(final ArrayList<StoreDetails> newStoreList) {
		// TODO Auto-generated method stub

		mStoreList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				String status = newStoreList.get(position).getStatus();
				String Store_Id = newStoreList.get(position).getStore_id();
				String store_name = newStoreList.get(position).getStore_name();
				String Store_Address = newStoreList.get(position).getStore_address();

				//Toast.makeText(mContext, "Name: " + newStoreList.get(position).getStore_name(),0).show();
				//
				//				if(status.equalsIgnoreCase("1")){
				//
				//					Intent intent = new Intent(mContext, LiveStreaming.class);
				//					intent.putExtra("STORE_ID",Store_Id);
				//					startActivity(intent);
				//				}
				//				else if(status.equalsIgnoreCase("2")){
				//
				//					Intent intent = new Intent(mContext, InTransitState.class);
				//					intent.putExtra("STORE_ID",Store_Id);
				//					startActivity(intent);
				//				}
				//				else if(status.equalsIgnoreCase("3")){
				//
				//					Intent intent = new Intent(mContext, StatusCompleteGalleryView.class);
				//					intent.putExtra("STORE_ID",Store_Id);
				//					startActivity(intent);
				//				}


				if(status.equalsIgnoreCase("0")){

					HashMap<String, String> param = new HashMap<String, String>();
					param.put("StoreName", store_name);
					param.put("StoreAdd", Store_Address);
					EventTracker.logEvent("Customer_Selected_Store_Installation_Details", param);


					Intent intent = new Intent(mContext, InstallationDetailView.class);
					intent.putExtra("STORE_ID", Store_Id);
					startActivity(intent);
				}
				else{
					HashMap<String, String> param = new HashMap<String, String>();
					param.put("StoreName", store_name);
					param.put("StoreAdd", Store_Address);
					EventTracker.logEvent("Customer_Selected_Store", param);


					Intent intent = new Intent(mContext, StoreStatus.class);
					intent.putExtra("STORE_ID",Store_Id);
					intent.putExtra("STORE_STATUS",status);
					intent.putExtra("STORE_NAME",store_name);
					intent.putExtra("STORE_ADDRESS", Store_Address);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			//logout customer
			//			session.logoutCustomer();
			//			Intent intent = new Intent(mContext, LoginGateway.class);
			//			startActivity(intent);
			//			finish();
			//			overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					EventTracker.logEvent("Customer_Logout_Btn_Pressed", false);
					session.logoutCustomer();
					StoreList.this.finish();
					Intent intet = new Intent(mContext,LoginGateway.class);
					startActivity(intet);
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);


				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}
		return true;
	}


	void showToast(String msg){

		Toast.makeText(mContext, ""+ msg, Toast.LENGTH_SHORT).show();
	}


	private void callStoreListApi() {
		// TODO Auto-generated method stub

		mLoginProgressBarStoreList.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("customer_id", mCustomer_ID);
		headers.put("auth_id", mAuth_Id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, STORE_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mLoginProgressBarStoreList.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseResponse(api_response);
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mLoginProgressBarStoreList.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}

	private void parseResponse(String api_response) {
		// TODO Auto-generated method stub

		try {

			//storeDetailsData = new ArrayList<StoreDetails>(); 
			JSONObject json = new JSONObject(api_response);
			mSuccess = json.getString("success");

			if(mSuccess.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");

				for(int i=0;i<data.length();i++){

					mStoreDetailModel = new StoreDetails();

					JSONObject storeDetails = data.getJSONObject(i);
					//mStoreNames.add(storeDetails.getString("store_name"));

					//to set the store details in Respective POJO
					mStoreDetailModel.setStore_id(storeDetails.getString("store_id"));
					mStoreDetailModel.setCity_name(storeDetails.getString("city_name"));
					mStoreDetailModel.setProject_name(storeDetails.getString("project_name"));
					mStoreDetailModel.setStatus(storeDetails.getString("status"));
					mStoreDetailModel.setStore_name(storeDetails.getString("store_name"));
					mStoreDetailModel.setProject_id(storeDetails.getString("project_id"));
					mStoreDetailModel.setStore_address(storeDetails.getString("store_address"));
					mStoreDetailModel.setStore_description(storeDetails.getString("store_description"));
					mStoreDetailModel.setStore_pincode(storeDetails.getString("store_pincode"));

					storeDetailsData.add(mStoreDetailModel);
				}

				adapter.notifyDataSetChanged();

			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");


				if(errorCode.equalsIgnoreCase(getResources().getString(R.string.invalid_auth_code))){

					showAlertDialog();
				}
				else{
					showToast(errorMessage);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutCustomer();
				StoreList.this.finish();

				Intent intet = new Intent(mContext,LoginGateway.class);
				startActivity(intet);
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);


			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}

	public void onBackPressed() {
		if(back_pressed+2000 >System.currentTimeMillis()){

			finish();
		}
		else{

			showToast("Press again to exit.");
			back_pressed = System.currentTimeMillis();
		}
	}
}
