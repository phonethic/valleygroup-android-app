package com.phonethics.valleygroup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;
import com.phonethics.model.DesignDetailModel;

public class RecceModuleChooseLocation extends ActionBarActivity {

	Context mContext;
	Button mNextBtn;
	RelativeLayout mImgRelativeView1;
	RelativeLayout mImgRelativeView2;
	RelativeLayout mImgRelativeView3;

	private  final int REQUEST_CODE_CUSTOMCAMERA = 8;

	private  final int REQUEST_CODE_POSTGALLERY_ONE = 1;
	private  final int REQUEST_CODE_POSTGALLERY_TWO = 2;
	private  final int REQUEST_CODE_POSTGALLERY_THREE = 3;

	private  final int REQUEST_CODE_POSTCROPGALLERY = 10;
	private  final int REQUEST_CODE_POSTCROPGALLERY_TWO = 20;
	private  final int REQUEST_CODE_POSTCROPGALLERY_THREE = 30;
	private  final int REQUEST_CODE_TAKE_PICTURE = 7;

	private String FILE_PATH="";
	private String FILE_NAME="";
	private String FILE_TYPE="";
	private byte[] mByteArr = null ;

	ImageView mDesignImage3;
	ImageView mDesignImage2;
	ImageView mDesignImage1;
	int position;
	private final int REQUEST_CODE_CUSTOMCAMERA_TWO = 18;
	private final int REQUEST_CODE_CUSTOMCAMERA_THREE = 28;

	ArrayList<DesignDetailModel> mDesignDetails;
	String store_id;
	String design_id;
	String member_id;
	String auth_id;
	int mItemPos;
	String mMember_ID;
	String mMember_Auth_id;
	HashMap<String, String> mMemberDetails = new HashMap<String, String>();
	SessionManager session;
	String DESIGN_UPLOAD_URL;
	ProgressBar mDesignImgUpload;
	String mSuccess;
	String mRecceId = "";
	ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recce_module_choose_location);

		mContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Choose Location");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
		mNextBtn = (Button) findViewById(R.id.mNextBtn);
		mImgRelativeView1 = (RelativeLayout) findViewById(R.id.mImgRelativeView1);
		mImgRelativeView2 = (RelativeLayout) findViewById(R.id.mImgRelativeView2);
		mImgRelativeView3 = (RelativeLayout) findViewById(R.id.mImgRelativeView3);

		mDesignImage3 = (ImageView) findViewById(R.id.mDesignImage3);
		mDesignImage2 = (ImageView) findViewById(R.id.mDesignImage2);
		mDesignImage1 = (ImageView) findViewById(R.id.mDesignImage1);
		mDesignImgUpload = (ProgressBar) findViewById(R.id.mDesignImgUpload);

		DESIGN_UPLOAD_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.recce_design);

		Bundle bundle=getIntent().getExtras();

		if(bundle!=null){

			//			mDesignDetails = bundle.getParcelable("DesignDetals");
			//			mItemPos = (bundle.getInt("position"));
			store_id =  bundle.getString("store_id");
			design_id =  bundle.getString("design_id");
		}

		mMemberDetails = session.getMemberDetails();
		mMember_ID = mMemberDetails.get(SessionManager.MEMBER_ID);
		mMember_Auth_id = mMemberDetails.get(SessionManager.MEMBER_AUTH_ID);

		mImgRelativeView1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				position = 1;
				showPicturePickerDialog(position);
			}
		});

		mImgRelativeView2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				position = 2;
				showPicturePickerDialog(position);
			}
		});

		mImgRelativeView3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				position = 3;
				showPicturePickerDialog(position);
			}
		});

		mNextBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(mContext, RecceModuleComments.class);
				intent.putExtra("STORE_ID", store_id);
				intent.putExtra("DESIGN_ID", design_id);
				intent.putExtra("MEMBER_ID", mMember_ID);
				intent.putExtra("AUTH_ID", mMember_Auth_id);

				if(mRecceId.length()>0){

					intent.putExtra("RECCE_ID", mRecceId);
				}
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					//logout customer
					session.logoutMember();
					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}
		return true;
	}


	private void showPicturePickerDialog(final int position) {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage("Choose an option")
		.setCancelable(true)
		.setPositiveButton("Take Picture",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				takePictureFromCustomCamera(position);
			}
		})
		.setNegativeButton("Gallery",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				openGallery(position);
			}
		});


		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	private void openGallery(int imgPos) {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");

		if(imgPos == 1){

			startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY_ONE);
		}
		else if(imgPos == 2){

			startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY_TWO);
		}
		else if(imgPos == 3){

			startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY_THREE);
		}

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			switch (requestCode) {

			case REQUEST_CODE_CUSTOMCAMERA:
				if(resultCode == Activity.RESULT_OK){

					try{
						Bitmap bitmap = null;

						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480);

						if(bitmap != null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							mByteArr = stream.toByteArray();
							cameraImageSave.deleteFromFile();
							cameraImageSave = null;
							bitmap = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDesignImage1.setImageBitmap(bmp);

						callImagePostingApi(1);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				break;

			case REQUEST_CODE_CUSTOMCAMERA_TWO:
				if(resultCode == Activity.RESULT_OK){

					try{
						Bitmap bitmap = null;

						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480);

						if(bitmap != null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							mByteArr = stream.toByteArray();
							cameraImageSave.deleteFromFile();
							cameraImageSave = null;
							bitmap = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDesignImage2.setImageBitmap(bmp);

						callImagePostingApi(2);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				break;


			case REQUEST_CODE_CUSTOMCAMERA_THREE:
				if(resultCode == Activity.RESULT_OK){

					try{
						Bitmap bitmap = null;

						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480);

						if(bitmap != null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							mByteArr = stream.toByteArray();
							cameraImageSave.deleteFromFile();
							cameraImageSave = null;
							bitmap = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDesignImage3.setImageBitmap(bmp);

						callImagePostingApi(3);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				break;

			case REQUEST_CODE_POSTGALLERY_ONE:
				if(resultCode == Activity.RESULT_OK) {
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(mContext, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQUEST_CODE_POSTCROPGALLERY);
				}

				break;

			case REQUEST_CODE_POSTCROPGALLERY:
				if(resultCode == Activity.RESULT_OK){
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";

					CameraImageSave cameraSaveImage = new CameraImageSave();

					String filePath = cameraSaveImage.getImagePath();


					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480);

						Toast.makeText(mContext, "" + bitmap, 0).show();
						if(bitmap!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							cameraSaveImage.deleteFromFile();
							mByteArr = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDesignImage1.setImageBitmap(bmp);

						callImagePostingApi(1);
					}
					catch(Exception e){
						Log.e("Exception","Exception at Create broacast class");
						e.printStackTrace();
					}
				}
				break;

			case REQUEST_CODE_POSTGALLERY_TWO:
				if(resultCode == Activity.RESULT_OK) {
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(mContext, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQUEST_CODE_POSTCROPGALLERY_TWO);
				}

				break;

			case REQUEST_CODE_POSTCROPGALLERY_TWO:
				if(resultCode == Activity.RESULT_OK){
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";

					CameraImageSave cameraSaveImage = new CameraImageSave();

					String filePath = cameraSaveImage.getImagePath();


					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480);

						Toast.makeText(mContext, "" + bitmap, 0).show();
						if(bitmap!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							cameraSaveImage.deleteFromFile();
							mByteArr = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDesignImage2.setImageBitmap(bmp);

						callImagePostingApi(2);
					}
					catch(Exception e){
						Log.e("Exception","Exception at Create broacast class");
						e.printStackTrace();
					}
				}
				break;

			case REQUEST_CODE_POSTGALLERY_THREE:
				if(resultCode == Activity.RESULT_OK) {
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(mContext, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQUEST_CODE_POSTCROPGALLERY_THREE);
				}

				break;

			case REQUEST_CODE_POSTCROPGALLERY_THREE:
				if(resultCode == Activity.RESULT_OK){
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";

					CameraImageSave cameraSaveImage = new CameraImageSave();

					String filePath = cameraSaveImage.getImagePath();


					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480);

						Toast.makeText(mContext, "" + bitmap, 0).show();
						if(bitmap!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							cameraSaveImage.deleteFromFile();
							mByteArr = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDesignImage3.setImageBitmap(bmp);

						callImagePostingApi(3);
					}
					catch(Exception e){
						Log.e("Exception","Exception at Create broacast class");
						e.printStackTrace();
					}
				}
				break;

			}


		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	private void takePictureFromCustomCamera(int imgPos) {
		try {

			Intent intent = new Intent(mContext,CameraView.class);
			intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);

			if(imgPos == 1){

				startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA);
			}
			else if(imgPos == 2){

				startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA_TWO);
			}
			else if(imgPos == 3){

				startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA_THREE);
			}

			//startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA);
		} catch (ActivityNotFoundException e) {
			Log.d("", "cannot take picture", e);
		}
	}


	private void callImagePostingApi(int fileCount) {
		// TODO Auto-generated method stub
		mDesignImgUpload.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("content-type", "application/json");

		JSONObject json=new JSONObject();
		try {


			json.put("store_id", store_id);
			json.put("design_id", design_id);
			json.put("member_id", mMember_ID);
			json.put("auth_id", mMember_Auth_id);

			if(mRecceId.length()>0){

				json.put("recce_id", mRecceId);
			}

			JSONArray fileArray=new JSONArray();
			JSONObject fileobject = new JSONObject();

			if(mByteArr!=null){
				String user_file=Base64.encodeToString(mByteArr, Base64.DEFAULT);
				fileobject.put("filename", FILE_NAME);
				fileobject.put("filetype", FILE_TYPE);
				fileobject.put("filecount", fileCount);
				fileobject.put("userfile", user_file);
				fileArray.put(fileobject);
				json.put("file",fileArray);
			}

			writeToFile(json.toString());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, DESIGN_UPLOAD_URL, json, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mDesignImgUpload.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				//parseResponse(api_response);


				try {

					JSONObject jsonObj = new JSONObject(api_response);
					mSuccess = jsonObj.getString("success");

					if(mSuccess.equalsIgnoreCase("true")){

						JSONObject data = jsonObj.getJSONObject("data");
						mRecceId = data.getString("recce_id");

					}else{

						String errorMessage = jsonObj.getString("message");
						String errorCode = jsonObj.getString("code");

						
						if(errorCode.equalsIgnoreCase("-116")){

							showAlertDialog();
						}
						else{
							
							showToast(errorMessage);
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mDesignImgUpload.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);
	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutMember();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	void showToast(String msg){

		Toast.makeText(mContext, ""+ msg, Toast.LENGTH_SHORT).show();
	}

	//write json to file

	private void writeToFile(String data) {

		try {
			File myFile = new File("/sdcard/imgupload.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();


		} 
		catch (Exception e) 
		{
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

}
