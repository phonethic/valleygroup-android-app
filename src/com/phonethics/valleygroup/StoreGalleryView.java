package com.phonethics.valleygroup;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class StoreGalleryView extends ActionBarActivity {

	static Context mContext;
	ViewPager mViewPager;
	ArrayList<String> mImageUrls;
	int mPosition;
	ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_gallery_view);

		mContext = this;
		mViewPager = (ViewPager) findViewById(R.id.mViewPager);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Store Gallery");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));


		Bundle bundle=getIntent().getExtras();


		if(bundle!=null){

			mPosition = bundle.getInt("Position");
			mImageUrls = bundle.getStringArrayList("ImagUrls");
		}

		mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),mContext, mImageUrls));
		mViewPager.setCurrentItem(mPosition);
	}



	class ViewPagerAdapter extends FragmentPagerAdapter{

		Context context;
		ArrayList<String> images = null;

		public ViewPagerAdapter(FragmentManager fm, Context context, ArrayList<String> images) {
			super(fm);
			// TODO Auto-generated constructor stub

			this.context = context;
			this.images = images;
		}

		@Override
		public Fragment getItem(int arg0) {
			// TODO Auto-generated method stub


			return SplashPagerFragment.newInstance(images.get(arg0));

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return images.size();
		}


	}

	public static class SplashPagerFragment extends Fragment {

		Context context;
		String images;


		public SplashPagerFragment()
		{

		}

		public static SplashPagerFragment newInstance(String images) {
			SplashPagerFragment fragmentFirst = new SplashPagerFragment();
			Bundle args = new Bundle();
			args.putString("images", images);
			fragmentFirst.setArguments(args);
			return fragmentFirst;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);

			images = getArguments().getString("images");

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub

			View view = inflater.inflate(R.layout.storegalleryimgholder, container, false);
			//ImageView imgView = (ImageView) view.findViewById(R.id.mStoreGalleryImg);
			final TouchImageView imgView = (TouchImageView) view.findViewById(R.id.mStoreGalleryImg);
			final ProgressBar mGalleryProgressBar = (ProgressBar) view.findViewById(R.id.mGalleryProgressBar);

			Picasso.with(mContext)
			.load(images)
			.placeholder(R.drawable.ic_launcher)
			.error(R.drawable.ic_launcher)
			.into(imgView,new Callback() {

				@Override
				public void onSuccess() {
					// TODO Auto-generated method stub
					//Toast.makeText(mContext, "Loaded", 0).show();
					mGalleryProgressBar.setVisibility(View.GONE);
				}

				@Override
				public void onError() {
					// TODO Auto-generated method stub
					mGalleryProgressBar.setVisibility(View.GONE);
					Toast.makeText(mContext, "Error in loading, please try again after sometime.", 0).show();
				}
			});
			//imgView.setImageResource(images);

			imgView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//
					//					PointF point = imgView.getScrollPosition();
					//					RectF rect = imgView.getZoomedRect();
					//					float currentZoom = imgView.getCurrentZoom();
					//					boolean isZoomed = imgView.isZoomed();

				}
			});

			return view;
		}

		@Override
		public void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
		}



	}

}
