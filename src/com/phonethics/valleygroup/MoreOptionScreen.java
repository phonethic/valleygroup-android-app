package com.phonethics.valleygroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phonethics.adapters.MilestoneListAdapter;

public class MoreOptionScreen extends ActionBarActivity {

	Button mFeedback;
	Button mCallStore;
	Button mMilestone;
	Context mContext;
	String STORE_MILESTONE_URL;
	HashMap<String, String> mCustomerDetails = new HashMap<String, String>();
	String mCustomer_ID;
	String mAuth_Id;
	SessionManager session;
	ProgressBar mMoreOptionProgressBar;
	String STORE_ID;
	String success;
	ArrayList<String> mStoreMilestone = new ArrayList<String>();
	MilestoneListAdapter mAdapter;
	ListView mMilestoneList;
	RelativeLayout mMoreOptionView;
	RelativeLayout mMilestoneLayout;
	Activity mAContext;
	String PARTICULAR_STORE_DETAILS_URL;

	//STORE DETAILS STRINGS
	String mCity_Name;
	String mEmail1;
	String mEmail2;
	String mProject_Description;
	String mProject_End_Date;
	String mProject_Name;
	String mProject_Start_Date;
	String mStore_Id;
	String mStore_name;
	String mStore_Status;
	String mTel_No1;
	String mTel_No2;
	ArrayList<String> mMobileNumbers = new ArrayList<String>();
	ArrayList<String> mEmails = new ArrayList<String>();
	Typeface mTypeface;
	ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more_option_screen);

		mContext = this;
		mAContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Store Options");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));

		mFeedback = (Button) findViewById(R.id.mFeedback);
		mCallStore = (Button) findViewById(R.id.mCallStore);
		mMilestone = (Button) findViewById(R.id.mMilestone);
		mMoreOptionProgressBar = (ProgressBar) findViewById(R.id.mMoreOptionProgressBar);
		mMilestoneList = (ListView) findViewById(R.id.mMilestoneList);
		mMoreOptionView = (RelativeLayout) findViewById(R.id.mMoreOptionView);
		mMilestoneLayout = (RelativeLayout) findViewById(R.id.mMilestoneLayout);
		mTypeface = Typeface.createFromAsset(getAssets(),"fonts/Arial_Bold.ttf");

		mFeedback.setTypeface(mTypeface);
		mCallStore.setTypeface(mTypeface);
		mMilestone.setTypeface(mTypeface);

		//STORE MILESTONE
		STORE_MILESTONE_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.store_milestone);

		//PARTICULAR STORE DETAILS
		PARTICULAR_STORE_DETAILS_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api) + getResources().getString(R.string.particular_store_detail);

		mCustomerDetails = session.getUserDetails();
		mCustomer_ID = mCustomerDetails.get(SessionManager.CUSTOMER_ID);
		mAuth_Id = mCustomerDetails.get(SessionManager.AUTH_ID);

		Bundle bundle=getIntent().getExtras();


		if(bundle!=null){

			STORE_ID = bundle.getString("STORE_ID");
		}

		callParticularStoreDetails();

		mFeedback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//showToast("Coming Soon");

			

				if(!mMoreOptionProgressBar.isShown()){
					
					if(mEmails.size()!=0){
						
						HashMap<String, String> param = new HashMap<String, String>();
						param.put("StoreName", mStore_name);
						param.put("StoreEmail", mEmails.get(0));
						EventTracker.logEvent("Customer_Feedback_Btn_Pressed", param);
					}

					if(mEmails.size()!=0){


						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL, new String[]{mEmails.get(0)});		  
						email.putExtra(Intent.EXTRA_SUBJECT, mStore_name);
						email.putExtra(Intent.EXTRA_TEXT, "");
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email, "Choose an option"));

					}
					else{

						Intent email = new Intent(Intent.ACTION_SEND);
						email.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@thevalleygroup1.zendesk.com"});		  
						email.putExtra(Intent.EXTRA_SUBJECT, mStore_name);
						email.putExtra(Intent.EXTRA_TEXT, "");
						email.setType("message/rfc822");
						startActivity(Intent.createChooser(email, "Choose an option"));
					}
				}
				else{

					showToast("Please wait...");
				}
			}
		});

		mCallStore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//showToast("Coming Soon");

				

				if(!mMoreOptionProgressBar.isShown()){
					
					if(mMobileNumbers.size()!=0){
						
						HashMap<String, String> param = new HashMap<String, String>();
						param.put("StoreName", mStore_name);
						param.put("StoreNumber",  mMobileNumbers.get(0));
						EventTracker.logEvent("Customer_CallStore_Btn_Pressed", param);

					}
				

					if(mMobileNumbers.size()>1){

						//					NumberListAdapter adapter = new NumberListAdapter(actContext, phoneNumberWithCode);
						//					numberlist.setAdapter(adapter);
						//					phoneNumberDialog.show();

						Intent call = new Intent(android.content.Intent.ACTION_DIAL);
						call.setData(Uri.parse("tel:" +mMobileNumbers.get(0)));
						startActivity(call);

					}
					else{

						if(mMobileNumbers.size()!=0){

							Log.d("NUMBER ", "NUMBER " + mMobileNumbers.get(0));
							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse("tel:" +mMobileNumbers.get(0)));
							startActivity(call);

						}
						else{

							showToast("Mobile number is not available for this store.");
						}
						//overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
				else{

					showToast("Please wait...");
				}
			}
		});

		mMilestone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//				callMilestoneApi();
				//				mMoreOptionView.setVisibility(View.GONE);
				//				mMilestoneLayout.setVisibility(View.VISIBLE);

				HashMap<String, String> param = new HashMap<String, String>();
				param.put("StoreName", mStore_name);
				EventTracker.logEvent("Customer_Installation_Details_Btn_Pressed", param);


				Intent intent = new Intent(mContext, InstallationDetailView.class);
				intent.putExtra("STORE_ID", STORE_ID);
				startActivity(intent);
			}
		});

	}

	private void callParticularStoreDetails() {
		// TODO Auto-generated method stub

		mMoreOptionProgressBar.setVisibility(View.VISIBLE);

		PARTICULAR_STORE_DETAILS_URL = PARTICULAR_STORE_DETAILS_URL + "?store_id=" +STORE_ID;

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("customer_id", mCustomer_ID);
		headers.put("auth_id", mAuth_Id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, PARTICULAR_STORE_DETAILS_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mMoreOptionProgressBar.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseParticularStoreResponse(api_response);
			}




		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mMoreOptionProgressBar.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}


	protected void callMilestoneApi() {
		// TODO Auto-generated method stub

		mMoreOptionProgressBar.setVisibility(View.VISIBLE);

		STORE_MILESTONE_URL = STORE_MILESTONE_URL + "?store_id=" +STORE_ID;

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("customer_id", mCustomer_ID);
		headers.put("auth_id", mAuth_Id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, STORE_MILESTONE_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mMoreOptionProgressBar.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseResponse(api_response);
			}




		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mMoreOptionProgressBar.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}

	void parseParticularStoreResponse(String api_response) {
		// TODO Auto-generated method stub

		try {

			JSONObject json = new JSONObject(api_response);
			String store_detail_success = json.getString("success");

			if(store_detail_success.equalsIgnoreCase("true")){

				JSONObject data = json.getJSONObject("data");

				mCity_Name = data.getString("city_name");
				mTel_No1 = data.getString("tel_no1");
				mStore_Status = data.getString("store_status");
				mProject_Name = data.getString("project_name");
				mTel_No2 = data.getString("tel_no2");
				mProject_End_Date = data.getString("project_end_date");
				mStore_Id = data.getString("store_id");
				mProject_Start_Date = data.getString("project_start_date");
				mEmail1 = data.getString("email1").trim();
				mStore_name = data.getString("store_name");
				mEmail2 = data.getString("email2").trim();
				mProject_Description = data.getString("project_description");

				// CHECK FOR MOBILE NUMBERS
				if(mTel_No1.length()>=10){
					mMobileNumbers.add(mTel_No1);
				}
				if(mTel_No2.length()>=10){
					mMobileNumbers.add(mTel_No2);	
				}


				//CHECK FOR EMPTY EMAIL
				if(mEmail1.length()!=0){
					mEmails.add(mEmail1);
				}
				if(mEmail2.length()!=0){
					mEmails.add(mEmail2);	
				}

			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");
				//showToast(errorMessage);

				if(errorCode.equalsIgnoreCase(getResources().getString(R.string.invalid_auth_code))){

					showAlertDialog();
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	protected void parseResponse(String api_response) {
		// TODO Auto-generated method stub

		JSONObject json;
		try {

			json = new JSONObject(api_response);
			success = json.getString("success");

			if(success.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");
				for(int i=0;i<data.length();i++){

					JSONObject milestoneDetails = data.getJSONObject(i);
					mStoreMilestone.add(milestoneDetails.getString("milestone"));
				}

				for(int j=0; j<mStoreMilestone.size();j++){

					Log.d("Milestones","Milestones " + mStoreMilestone.get(j));
				}

//				mAdapter = new MilestoneListAdapter(mAContext, mStoreMilestone);
//				mMilestoneList.setAdapter(mAdapter);
			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");
				
				//showToast(errorMessage);

				if(errorCode.equalsIgnoreCase(getResources().getString(R.string.invalid_auth_code))){

					showAlertDialog();
				}
				else{
					showToast(errorMessage);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void showToast(String message) {
		// TODO Auto-generated method stub

		Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			//logout customer
			//			session.logoutCustomer();
			//
			//			Intent intent = new Intent(mContext, LoginGateway.class);
			//			ComponentName cn = intent.getComponent();
			//			Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
			//			mContext.startActivity(mainIntent);
			//			finish();
			//			overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					EventTracker.logEvent("Customer_Logout_Btn_Pressed", false);
					session.logoutCustomer();

					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);


				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}
		return true;
	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutCustomer();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		EventTracker.startLocalyticsSession(mContext);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}
}
