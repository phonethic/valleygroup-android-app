package com.phonethics.valleygroup;

import java.io.ByteArrayOutputStream;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class DigitalSignature extends ActionBarActivity implements OnTouchListener {

	Context mContext;
	DrawingView    mDrawing;   
	private Paint  mPaint;
	Bitmap finalBitmap = null;
	Button mDoneBtn;
	RelativeLayout mParentLayout;
	android.support.v7.app.ActionBar mActionBar;
	Canvas newCanvas;
	Button mResetBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_digital_signature);

		mContext = this;
		mActionBar = getSupportActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
		mActionBar.setTitle("Retailer Approval");
		mParentLayout = (RelativeLayout) findViewById(R.id.mParentLayout);
		mDoneBtn = (Button) findViewById(R.id.mDoneBtn);
		mResetBtn = (Button) findViewById(R.id.mResetBtn);
		mDrawing = new DrawingView(this);

		RelativeLayout l1 = (RelativeLayout)findViewById(R.id.signView);
		l1.addView(mDrawing);

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(Color.BLACK);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(12); 

		mDoneBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//convertng to byte array
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();

				Intent returnIntent = new Intent();
				returnIntent.putExtra("SIGNATURE", byteArray);
				returnIntent.putExtra("result",20);
				setResult(RESULT_OK,returnIntent);
				finish();


			}
		});

		mResetBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				newCanvas.drawColor(Color.parseColor("#ebebeb"));

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.digital_signature, menu);
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	public class DrawingView extends View {

		public int width;
		public  int height;
		private Bitmap  mBitmap;
		//private Canvas  mCanvas;
		private Path    mPath;
		private Paint   mBitmapPaint;
		Context context;
		private Paint circlePaint;
		private Path circlePath;

		public DrawingView(Context c) {
			super(c);
			context=c;
			mPath = new Path();
			mBitmapPaint = new Paint(Paint.DITHER_FLAG);  
			circlePaint = new Paint();
			circlePath = new Path();
			circlePaint.setAntiAlias(true);
			circlePaint.setColor(Color.BLUE);
			circlePaint.setStyle(Paint.Style.STROKE);
			circlePaint.setStrokeJoin(Paint.Join.MITER);
			circlePaint.setStrokeWidth(4f); 


		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			super.onSizeChanged(w, h, oldw, oldh);

			mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
			newCanvas = new Canvas(mBitmap);


		}
		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);

			canvas.drawBitmap( mBitmap, 0, 0, mBitmapPaint);

			canvas.drawPath( mPath,  mPaint);

			canvas.drawPath( circlePath,  circlePaint);
		}

		private float mX, mY;
		private static final float TOUCH_TOLERANCE = 4;

		private void touch_start(float x, float y) {
			mPath.reset();
			mPath.moveTo(x, y);
			mX = x;
			mY = y;
		}
		private void touch_move(float x, float y) {
			float dx = Math.abs(x - mX);
			float dy = Math.abs(y - mY);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
				mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
				mX = x;
				mY = y;

				circlePath.reset();
				circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
			}
		}
		private void touch_up() {
			mPath.lineTo(mX, mY);
			circlePath.reset();
			// commit the path to our offscreen
			newCanvas.drawPath(mPath,  mPaint);
			// kill this so we don't double draw
			mPath.reset();

			finalBitmap = mBitmap;
			//Toast.makeText(getApplicationContext(), "Hello Done " + mBitmap.toString(), 0).show();
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			float x = event.getX();
			float y = event.getY();

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				touch_start(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				touch_move(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				touch_up();
				invalidate();

				break;
			}
			return true;
		}  
	}

}
