package com.phonethics.valleygroup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;

public class SplashMain extends Activity {
	
	 // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    Activity mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_main);
		
		mContext = this;
		EventTracker.startLocalyticsSession(mContext);
		EventTracker.logEvent("Valley Group app started", false);
		
	     new Handler().postDelayed(new Runnable() {
	    	 
	            /*
	             * Showing splash screen with a timer. This will be useful when you
	             * want to show case your app logo / company
	             */
	 
	            @Override
	            public void run() {
	                // This method will be executed once the timer is over
	                // Start your app main activity
	                Intent i = new Intent(SplashMain.this, SplashScreen.class);
	                startActivity(i);
	            	overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	                // close this activity
	                finish();
	            }
	        }, SPLASH_TIME_OUT);
	}

	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}

	
}
