package com.phonethics.valleygroup;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class LoginGateway extends ActionBarActivity {

	ActionBar mActionBar;
	//	Button mFrontLineLogin;
	//	Button mCustomerLogin;

	RelativeLayout mFrontLineLogin;
	RelativeLayout mCustomerLogin;

	Typeface mTypeFace;
	ScrollView mLoginScrollView;
	RelativeLayout mToggleView;
	Button mBtnLogin;
	EditText mPassword;
	EditText mMobileNo;
	Context mContext;
	ViewFlipper mViewFlipper;

	String LOGIN_URL;
	JSONObject jsonObj;
	String success;
	String message;
	ProgressBar mLoginProgressBar;
	SessionManager mSessionManager;
	String mMobileNum;
	String mPass;
	boolean mIsCustomer;
	String mCustomer_Id;
	String mAuth_Id;
	boolean mIsMember;
	String TEAM_LOGIN_URL;
	String mTeamMember_Id;
	String mTeamMember_Auth_Id;
	TextView headerTextCustomer;
	TextView headerTextTeam;
	Typeface mPageTitle;
	TextView mPhonethicsLink;
	ImageView loginIcon;
	private long back_pressed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_gateway);

		mContext = this;
		mActionBar = getSupportActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
		mActionBar.setTitle("Welcome");

		mTypeFace = Typeface.createFromAsset(getAssets(),"fonts/ABALC.ttf");
		mPageTitle =  Typeface.createFromAsset(getAssets(),"fonts/Arial_Bold.ttf");

		//		mFrontLineLogin = (Button) findViewById(R.id.mFrontLineLogin);
		//		mCustomerLogin = (Button) findViewById(R.id.mCustomerLogin);

		mFrontLineLogin = (RelativeLayout) findViewById(R.id.mFrontLineLogin);
		mCustomerLogin = (RelativeLayout) findViewById(R.id.mCustomerLogin);

		//	mLoginScrollView = (ScrollView) findViewById(R.id.mLoginScrollView);
		mToggleView = (RelativeLayout) findViewById(R.id.mToggleView);
		mBtnLogin = (Button) findViewById(R.id.mBtnLogin);
		mMobileNo = (EditText) findViewById(R.id.mMobileNo);
		mPassword = (EditText) findViewById(R.id.mPassword);
		mViewFlipper = (ViewFlipper) findViewById(R.id.mViewFlipper);
		mLoginProgressBar = (ProgressBar) findViewById(R.id.mLoginProgressBar);
		mPhonethicsLink = (TextView) findViewById(R.id.mPhonethicsLink);
		mSessionManager = new SessionManager(mContext);
		loginIcon = (ImageView) findViewById(R.id.loginIcon);
		//		headerTextTeam = (TextView) findViewById(R.id.headerTextTeam);
		//		headerTextCustomer = (TextView) findViewById(R.id.headerTextCustomer);

		//		headerTextCustomer.setTypeface(mPageTitle);
		//		headerTextTeam.setTypeface(mPageTitle);

		//		mFrontLineLogin.setTypeface(mTypeFace,Typeface.BOLD);
		//		mCustomerLogin.setTypeface(mTypeFace,Typeface.BOLD);
		mBtnLogin.setTypeface(mPageTitle,Typeface.BOLD);

		//Url for customer login
		LOGIN_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+ getResources().getString(R.string.customer_api)+ getResources().getString(R.string.login);

		//Url for frontline team login
		TEAM_LOGIN_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+ getResources().getString(R.string.member_api)+ getResources().getString(R.string.login_member);

		mCustomerLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//				mLoginScrollView.setVisibility(View.VISIBLE);
				//				mToggleView.setVisibility(View.GONE);

				mIsCustomer = true;
				mActionBar.setTitle("Customer Login");

				//				headerTextCustomer.setVisibility(View.VISIBLE);
				//				headerTextTeam.setVisibility(View.GONE);

				mViewFlipper.setInAnimation(mContext,R.anim.in_from_right);
				mViewFlipper.setOutAnimation(mContext,R.anim.out_to_left);
				mViewFlipper.showNext();
				inValidateActionBar();
				loginIcon.setImageResource(R.drawable.customer_icon);


			}
		});

		mFrontLineLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//				mIsCustomer = true;
				//				mActionBar.setTitle("Member Login");
				//				mViewFlipper.setInAnimation(mContext,R.anim.in_from_right);
				//				mViewFlipper.setOutAnimation(mContext,R.anim.out_to_left);
				//				mViewFlipper.showNext();
				//				inValidateActionBar();
				mIsMember = true;
				mActionBar.setTitle("Frontline Team Login");
				//				headerTextCustomer.setVisibility(View.GONE);
				//				headerTextTeam.setVisibility(View.VISIBLE);

				mViewFlipper.setInAnimation(mContext,R.anim.in_from_right);
				mViewFlipper.setOutAnimation(mContext,R.anim.out_to_left);
				mViewFlipper.showNext();
				inValidateActionBar();

				loginIcon.setImageResource(R.drawable.frontline_team_icon);
			}
		});

		//login btn click listener
		mBtnLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				if(mMobileNo.getText().toString().length() == 0){

					showToast("Mobile number cannot be empty");
				}
				else if(mPassword.getText().toString().length() == 0){

					showToast("Password cannot be empty");
				}
				else if(mMobileNo.getText().toString().length()<10)
				{
					showToast("Please enter correct Mobile no.");
				}
				else{

					mLoginProgressBar.setVisibility(View.VISIBLE);
					mMobileNum = mMobileNo.getText().toString();
					mPass = mPassword.getText().toString();

					try {

						//make JSON for POSTING username & pasword

						jsonObj = new JSONObject();
						jsonObj.put("mobile", mMobileNo.getText().toString());
						jsonObj.put("password", mPassword.getText().toString());

					}catch(JSONException jsexp){

						jsexp.printStackTrace();

					}
					catch (Exception e) {

						e.printStackTrace();
					}


					// Call Login API

					final HashMap<String, String> headers = new HashMap<String, String>();
					headers.put("X-API-KEY", "vgiapp");

					if(mIsCustomer){

						EventTracker.logEvent("Customer_Login_Btn_Pressed", false);

						RequestQueue queue = Volley.newRequestQueue(mContext);
						JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, LOGIN_URL, jsonObj, new Response.Listener<JSONObject>() {

							@Override
							public void onResponse(JSONObject response) {
								// TODO Auto-generated method stub

								mLoginProgressBar.setVisibility(View.GONE);
								String api_response = response.toString();
								Log.d("RESPONSE ","RESPONSE "  + api_response);
								parseResponse(api_response);

								if(success.equalsIgnoreCase("false")){

									EventTracker.logEvent("Customer_Login_Failed", false);
									showToast(message);

								}
								else{

									EventTracker.logEvent("Customer_Login_Success", false);
									showToast(message);
									mSessionManager.createLoginSessionCustomer(mMobileNum, mPass, mCustomer_Id, mAuth_Id);

									Intent intent = new Intent(mContext, StoreList.class);
									startActivity(intent);
									finish();
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

								}

							}



						}, new Response.ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError error) {
								// TODO Auto-generated method stub

								mLoginProgressBar.setVisibility(View.GONE);

								EventTracker.logEvent("Customer_Login_Failed", false);
								if(error instanceof NetworkError){
									showToast(getResources().getString(R.string.Network));
								}else if(error instanceof AuthFailureError){
									showToast(getResources().getString(R.string.Authentication));
								}else if(error instanceof ServerError ){
									showToast(getResources().getString(R.string.Server));
								}else if(error instanceof NoConnectionError){
									showToast(getResources().getString(R.string.Internet));
								}else if(error instanceof TimeoutError){
									showToast(getResources().getString(R.string.TimeOut));
								}else if(error instanceof ParseError){
									showToast(getResources().getString(R.string.Parse));
								}
							}
						})
						{
							@Override
							public Map<String, String> getHeaders() throws AuthFailureError{

								// TODO Auto-generated method stub
								return headers;
							}
						};

						queue.add(jsObjRequest);

					}

					if(mIsMember){


						RequestQueue queue = Volley.newRequestQueue(mContext);
						JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, TEAM_LOGIN_URL, jsonObj, new Response.Listener<JSONObject>() {

							@Override
							public void onResponse(JSONObject response) {
								// TODO Auto-generated method stub

								mLoginProgressBar.setVisibility(View.GONE);
								String api_response = response.toString();
								Log.d("RESPONSE ","RESPONSE "  + api_response);
								parseResponseTeamMembers(api_response);

								if(success.equalsIgnoreCase("false")){

									showToast(message);
								}
								else{

									showToast(message);
									mSessionManager.createLoginSessionFrontlineMember(mMobileNum, mPass, mTeamMember_Id, mTeamMember_Auth_Id);

									Intent intent = new Intent(mContext, MemberModuleSelection.class);
									startActivity(intent);
									finish();
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								}

							}


						}, new Response.ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError error) {
								// TODO Auto-generated method stub

								mLoginProgressBar.setVisibility(View.GONE);

								if(error instanceof NetworkError){
									showToast(getResources().getString(R.string.Network));
								}else if(error instanceof AuthFailureError){
									showToast(getResources().getString(R.string.Authentication));
								}else if(error instanceof ServerError ){
									showToast(getResources().getString(R.string.Server));
								}else if(error instanceof NoConnectionError){
									showToast(getResources().getString(R.string.Internet));
								}else if(error instanceof TimeoutError){
									showToast(getResources().getString(R.string.TimeOut));
								}else if(error instanceof ParseError){
									showToast(getResources().getString(R.string.Parse));
								}
							}
						})
						{
							@Override
							public Map<String, String> getHeaders() throws AuthFailureError{

								// TODO Auto-generated method stub
								return headers;
							}
						};

						queue.add(jsObjRequest);
					}

				}
			}
		});

		mPhonethicsLink.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				EventTracker.logEvent("Phonethics_Btn_Pressed", false);
				Intent intent = new Intent(mContext, WebLinkView.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		if(mIsCustomer){

			MenuItem extra=menu.add("SwitchView").setIcon(R.drawable.switch_1);
			MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		}
		else if(mIsMember){

			MenuItem extra=menu.add("SwitchView").setIcon(R.drawable.switch_2);
			MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("SwitchView"))
		{
			//showToast("Logout pressed");

			//logout customer
			mActionBar.setTitle("Welcome");

			//			headerTextCustomer.setVisibility(View.GONE);
			//			headerTextTeam.setVisibility(View.GONE);

			mViewFlipper.setInAnimation(mContext,R.anim.in_from_left);
			mViewFlipper.setOutAnimation(mContext,R.anim.out_to_right);
			mViewFlipper.showPrevious();
			mIsCustomer = false;
			mIsMember = false;
			inValidateActionBar();
		}
		return true;
	}


	private void parseResponse(String api_response) {
		// TODO Auto-generated method stub


		try {

			JSONObject json = new JSONObject(api_response);
			success = json.getString("success");
			message = json.getString("message");

			JSONObject data = json.getJSONObject("data");
			mCustomer_Id = data.getString("id");
			mAuth_Id = data.getString("auth_code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseResponseTeamMembers(
			String api_response) {
		// TODO Auto-generated method stub

		try {

			JSONObject json = new JSONObject(api_response);
			success = json.getString("success");
			message = json.getString("message");

			JSONObject data = json.getJSONObject("data");
			mTeamMember_Id = data.getString("id");
			mTeamMember_Auth_Id = data.getString("auth_code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	void showToast(String msg){

		Toast.makeText(mContext, ""+ msg, Toast.LENGTH_SHORT).show();
	}

	void inValidateActionBar(){

		this.supportInvalidateOptionsMenu();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}

	public void onBackPressed() {

//		mMobileNo.clearFocus();
//		mPassword.clearFocus();
//		mPhonethicsLink.setVisibility(View.VISIBLE);
		if(back_pressed+2000 >System.currentTimeMillis()){

			finish();
		}
		else{

			showToast("Press again to exit.");
			back_pressed = System.currentTimeMillis();
		}
	}


}
