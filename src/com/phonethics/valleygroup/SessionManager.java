package com.phonethics.valleygroup;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {

	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "AndroidHivePref";

	// All Shared Preferences Keys
	private static final String IS_LOGIN_CUSTOMER = "IsLoggedInCustomer";

	private static final String IS_FRONTLINE_MEMBER_LOGGEDIN = "IsLoggedInFrontlineMember";

	//Customer Mobile NUM name (make variable public to access from outside)
	public static final String MOBILE_NUM = "mobile";
	//Customer Password address (make variable public to access from outside)
	public static final String PASSWORD = "password";
	//Customer Customer_Id
	public static final String CUSTOMER_ID = "customer_id";
	//Customer Auth_Id
	public static final String AUTH_ID = "auth_id";

	//FrontLine Member Mobile NUM name (make variable public to access from outside)
	public static final String MEMBER_MOBILE_NUM = "member_mobile";
	//Customer Password address (make variable public to access from outside)
	public static final String MEMBER_PASSWORD = "member_password";
	//Customer Customer_Id
	public static final String MEMBER_ID = "member_id";
	//Customer Auth_Id
	public static final String MEMBER_AUTH_ID = "member_auth_id";


	// Constructor
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Create login session customer
	 * */
	public void createLoginSessionCustomer(String mobile_num, String password, String customer_id, String auth_id){
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN_CUSTOMER, true);

		// Storing mobile_num in pref
		editor.putString(MOBILE_NUM, mobile_num);

		// Storing password in pref
		editor.putString(PASSWORD, password);

		//storing customer_id
		editor.putString(CUSTOMER_ID, customer_id);

		//storing auth_id
		editor.putString(AUTH_ID, auth_id);

		// commit changes
		editor.commit();
	}   

	/**
	 * Create login session Member Login
	 * */
	public void createLoginSessionFrontlineMember(String mobile_num, String password, String member_id, String auth_id){
		// Storing login value as TRUE
		editor.putBoolean(IS_FRONTLINE_MEMBER_LOGGEDIN, true);

		// Storing mobile in pref
		editor.putString(MEMBER_MOBILE_NUM, mobile_num);

		// Storing password in pref
		editor.putString(MEMBER_PASSWORD, password);

		//storing member_id
		editor.putString(MEMBER_ID, member_id);

		//storing member_auth_id
		editor.putString(MEMBER_AUTH_ID, auth_id);

		// commit changes
		editor.commit();

		Log.d("AUTH_ID Pref","AUTH_ID " +  auth_id);
		Log.d("Member_ID Pref","Member_ID " +  member_id);
	}   

	/**
	 * Get stored session data customer
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// Customer mobile
		user.put(MOBILE_NUM, pref.getString(MOBILE_NUM, null));

		// Customer password
		user.put(PASSWORD, pref.getString(PASSWORD, null));

		// customer_id
		user.put(CUSTOMER_ID, pref.getString(CUSTOMER_ID, null));

		// Auth_id
		user.put(AUTH_ID, pref.getString(AUTH_ID, null));

		// return user
		return user;
	}

	// Get Login State Customer
	public boolean isLoggedInCustomer(){
		return pref.getBoolean(IS_LOGIN_CUSTOMER, false);
	}


	/**
	 * Get stored session data members
	 * */
	public HashMap<String, String> getMemberDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// Customer mobile
		user.put(MEMBER_MOBILE_NUM, pref.getString(MEMBER_MOBILE_NUM, null));

		// Customer password
		user.put(MEMBER_PASSWORD, pref.getString(MEMBER_PASSWORD, null));

		// customer_id
		user.put(MEMBER_ID, pref.getString(MEMBER_ID, null));

		// Auth_id
		user.put(MEMBER_AUTH_ID, pref.getString(MEMBER_AUTH_ID, null));

		// return user
		return user;
	}

	// Get Login State FRONTLINE_MEMBER
	public boolean isLoggedInFrontlineMember(){
		return pref.getBoolean(IS_FRONTLINE_MEMBER_LOGGEDIN, false);
	}

	/**
	 * Clear session details customer
	 * */
	public void logoutCustomer(){
		// Clearing all data from Shared Preferences
		//editor.clear();

		editor.remove(MOBILE_NUM);
		editor.remove(PASSWORD);
		editor.remove(CUSTOMER_ID);
		editor.remove(AUTH_ID);
		editor.remove(IS_LOGIN_CUSTOMER);
		editor.commit();

	}

	/**
	 * Clear session details member
	 * */
	public void logoutMember(){
		// Clearing all data from Shared Preferences
		//editor.clear();

		editor.remove(MEMBER_MOBILE_NUM);
		editor.remove(MEMBER_PASSWORD);
		editor.remove(MEMBER_ID);
		editor.remove(MEMBER_AUTH_ID);
		editor.remove(IS_FRONTLINE_MEMBER_LOGGEDIN);
		editor.commit();

	}

}
