package com.phonethics.valleygroup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phonethics.adapters.AnswerModel;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;
import com.phonethics.model.InstallationQuestionModel;

public class InstallationQuestionsView extends ActionBarActivity {

	Context mContext;
	SessionManager session;
	Button mNextBtn;
	Button mPreviousBtn;
	String store_id;
	String category_id;
	ProgressBar mQuestionViewProgressBar;
	String mMember_ID;
	String mMember_Auth_id;
	HashMap<String, String> mMemberDetails = new HashMap<String, String>();
	String QUESTION_GET_URL;
	String mSuccess;
	InstallationQuestionModel questionModel ;
	ArrayList<String> totalOptions = new ArrayList<String>();
	ArrayList<InstallationQuestionModel> mquestionModelObj = new ArrayList<InstallationQuestionModel>();
	int mRecordCount = 0;
	Button mSubmitButton;
	String mMessage;
	LinearLayout mFinalButtonView;
	Button mFinalPrevBtn;
	LinearLayout mButtonView;
	LinearLayout mImageAddView;
	ImageView mDamageImage;
	ImageView mApprovalSignImage;
	byte[] finalSignature = null;
	private byte[] mByteArr = null ;

	private  final int REQUEST_CODE_POSTGALLERY = 1;
	private  final int REQUEST_CODE_CUSTOMCAMERA = 8;
	private  final int REQUEST_CODE_TAKE_PICTURE = 7;
	private  final int REQUEST_CODE_POSTCROPGALLERY = 10;

	private String FILE_PATH="";
	private String FILE_NAME="";
	private String FILE_TYPE="image/jpeg";

	//for URLS
	String APPROVAL_IMG_URL;

	//for approval id
	String mApprovalId = null;

	//Boolean for approval & damage images
	boolean isApproval;
	boolean isDamage;

	//Type Tick
	LinearLayout mSingleChoice;
	TextView mQuesTypeCheck;
	CheckBox mCheckBox1;
	CheckBox mCheckBox2;
	CheckBox mCheckBox3;
	CheckBox mCheckBox4;
	CheckBox mCheckBox5;
	TextView mOptionType1;
	TextView mOptionType2;
	TextView mOptionType3;
	TextView mOptionType4;
	TextView mOptionType5;
	RelativeLayout mOptionTypeCheckRel5,mSingleChoiceOptionsRelative3,mOptionTypeCheckRel4;

	//Type Tick N Text
	LinearLayout mParentTicPlusTxt;
	TextView mQuesTickNText;
	CheckBox mCheckBoxTicNText1;
	CheckBox mCheckBoxTicNText2;
	CheckBox mCheckBoxTicNText3;
	CheckBox mCheckBoxTicNText4;
	CheckBox mCheckBoxTicNText5;
	TextView mOptionTypeTicNText1;
	TextView mOptionTypeTicNText2;
	TextView mOptionTypeTicNText3;
	TextView mOptionTypeTicNText4;
	TextView mOptionTypeTicNText5;
	RelativeLayout mOptionTicNTextRel4;
	RelativeLayout mOptionTicNTextRel5;
	EditText mEditTickNTxt;
	String mTickPlusText;

	//Type text only
	LinearLayout mParentText;
	TextView mQuesText;
	EditText mEditTxt;
	String mEditTick;

	//To maintain states of Answer
	String mQuestionType;
	String mOptionType;
	Map<String, List<String>> mAnswerOptions = new HashMap<String, List<String>>();
	AnswerModel mAnswerModel;
	ArrayList<AnswerModel> mAnswerObjList = new ArrayList<AnswerModel>();
	Map<String,String> mMap = new HashMap<String,String>();
	ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_installation_questions_view);

		mContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
		mPreviousBtn = (Button) findViewById(R.id.mPreviousBtn);
		mNextBtn = (Button) findViewById(R.id.mNextBtn);
		mQuestionViewProgressBar = (ProgressBar) findViewById(R.id.mQuestionViewProgressBar);
		mSubmitButton = (Button) findViewById(R.id.mSubmitButton);
		mFinalButtonView = (LinearLayout) findViewById(R.id.mFinalButtonView);
		mFinalPrevBtn = (Button) findViewById(R.id.mFinalPrevBtn);
		mButtonView = (LinearLayout) findViewById(R.id.mButtonView);
		mImageAddView = (LinearLayout) findViewById(R.id.mImageAddView);
		mApprovalSignImage = (ImageView) findViewById(R.id.mApprovalSignImage);
		mDamageImage = (ImageView) findViewById(R.id.mDamageImage);

		//for tick type questions
		mSingleChoice = (LinearLayout) findViewById(R.id.mSingleChoice);
		mQuesTypeCheck = (TextView) findViewById(R.id.mQuesTypeCheck);
		mCheckBox1 = (CheckBox) findViewById(R.id.mCheckBox1);
		mCheckBox2 = (CheckBox) findViewById(R.id.mCheckBox2);
		mCheckBox3 = (CheckBox) findViewById(R.id.mCheckBox3);
		mCheckBox4 = (CheckBox) findViewById(R.id.mCheckBox4);
		mCheckBox5 = (CheckBox) findViewById(R.id.mCheckBox5);
		mOptionType1 = (TextView) findViewById(R.id.mOptionType1);
		mOptionType2 = (TextView) findViewById(R.id.mOptionType2);
		mOptionType3 = (TextView) findViewById(R.id.mOptionType3);
		mOptionType4 = (TextView) findViewById(R.id.mOptionType4);
		mOptionType5 = (TextView) findViewById(R.id.mOptionType5);
		mOptionTypeCheckRel5 = (RelativeLayout) findViewById(R.id.mOptionTypeCheckRel5);
		mOptionTypeCheckRel4 = (RelativeLayout) findViewById(R.id.mOptionTypeCheckRel4);
		mSingleChoiceOptionsRelative3 = (RelativeLayout) findViewById(R.id.mSingleChoiceOptionsRelative3);

		//for tick n text type questions
		mParentTicPlusTxt = (LinearLayout) findViewById(R.id.mParentTicPlusTxt);
		mQuesTickNText = (TextView) findViewById(R.id.mQuesTickNText);
		mCheckBoxTicNText1 = (CheckBox) findViewById(R.id.mCheckBoxTicNText1);
		mCheckBoxTicNText2 = (CheckBox) findViewById(R.id.mCheckBoxTicNText2);
		mCheckBoxTicNText3 = (CheckBox) findViewById(R.id.mCheckBoxTicNText3);
		mCheckBoxTicNText4 = (CheckBox) findViewById(R.id.mCheckBoxTicNText4);
		mCheckBoxTicNText5 = (CheckBox) findViewById(R.id.mCheckBoxTicNText5);
		mOptionTypeTicNText1 = (TextView) findViewById(R.id.mOptionTypeTicNText1);
		mOptionTypeTicNText2 = (TextView) findViewById(R.id.mOptionTypeTicNText2);
		mOptionTypeTicNText3 = (TextView) findViewById(R.id.mOptionTypeTicNText3);
		mOptionTypeTicNText4 = (TextView) findViewById(R.id.mOptionTypeTicNText4);
		mOptionTypeTicNText5 = (TextView) findViewById(R.id.mOptionTypeTicNText5);
		mOptionTicNTextRel4 = (RelativeLayout) findViewById(R.id.mOptionTicNTextRel4);
		mOptionTicNTextRel5 = (RelativeLayout) findViewById(R.id.mOptionTicNTextRel5);
		mEditTickNTxt = (EditText) findViewById(R.id.mEditTickNTxt);

		//for text type questions
		mParentText = (LinearLayout) findViewById(R.id.mParentText);
		mQuesText = (TextView) findViewById(R.id.mQuesText);
		mEditTxt = (EditText) findViewById(R.id.mEditTxt);


		Bundle bundle=getIntent().getExtras();
		if(bundle!=null){

			store_id = bundle.getString("STORE_ID");
			category_id = bundle.getString("CATEGORY_ID");
		}

		mMemberDetails = session.getMemberDetails();
		mMember_ID = mMemberDetails.get(SessionManager.MEMBER_ID);
		mMember_Auth_id = mMemberDetails.get(SessionManager.MEMBER_AUTH_ID);

		QUESTION_GET_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+ getResources().getString(R.string.store_api) + getResources().getString(R.string.inspection_questions);

		QUESTION_GET_URL = QUESTION_GET_URL + "?store_id=" + store_id + "&category_id=" + category_id;

		APPROVAL_IMG_URL = getResources().getString(R.string.base_url)+ getResources().getString(R.string.active_api) +
				getResources().getString(R.string.store_api) + getResources().getString(R.string.inspection_img);

		callAllQuestionsApi();

		mNextBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//getAnswer();

				//setAnswer();

				//mAnswerModel = new AnswerModel();


				if(mRecordCount>=mquestionModelObj.size()-1){

					//showToast("No more questions to display.");
					//mSubmitButton.setVisibility(View.VISIBLE);
					mFinalButtonView.setVisibility(View.VISIBLE);
					mButtonView.setVisibility(View.GONE);

					//for ques layout gone on image upload
					mParentText.setVisibility(View.GONE);
					mParentTicPlusTxt.setVisibility(View.GONE);
					mSingleChoice.setVisibility(View.GONE);
					mImageAddView.setVisibility(View.VISIBLE);
				}
				else{



					mRecordCount++;

					mCheckBox1.setChecked(false);
					mCheckBox2.setChecked(false);
					mCheckBox3.setChecked(false);
					mCheckBox4.setChecked(false);
					mCheckBox5.setChecked(false);

					mCheckBoxTicNText1.setChecked(false);
					mCheckBoxTicNText2.setChecked(false);
					mCheckBoxTicNText3.setChecked(false);
					mCheckBoxTicNText4.setChecked(false);
					mCheckBoxTicNText5.setChecked(false);

					setAnswer();
					showQuestion();
				}

			}
		});

		mPreviousBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//getAnswer();

				//mAnswerModel = new AnswerModel();
				//mSubmitButton.setVisibility(View.GONE);
				if(mRecordCount<=0){

					showToast("No more questions to display.");
				}
				else{


					mRecordCount--;

					mCheckBox1.setChecked(false);
					mCheckBox2.setChecked(false);
					mCheckBox3.setChecked(false);
					mCheckBox4.setChecked(false);
					mCheckBox5.setChecked(false);

					mCheckBoxTicNText1.setChecked(false);
					mCheckBoxTicNText2.setChecked(false);
					mCheckBoxTicNText3.setChecked(false);
					mCheckBoxTicNText4.setChecked(false);
					mCheckBoxTicNText5.setChecked(false);
					setAnswer();
					showQuestion();
				}

			}
		});


		//clicks for check boxes

		mCheckBox1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mCheckBox1.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType1.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType1.getText().toString(), "1");
				}
				else{

					//showToast("Uncheck 1");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType1.getText().toString(), "0");
				}
			}
		});
		mCheckBox2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mCheckBox2.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType2.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType2.getText().toString(), "1");
				}
				else{

					//showToast("Uncheck 2");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType2.getText().toString(), "0");
				}
			}
		});
		mCheckBox3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mCheckBox3.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType3.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType3.getText().toString(), "1");
				}
				else{

					//showToast("Uncheck 3");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType3.getText().toString(), "0");
				}
			}
		});
		mCheckBox4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mCheckBox4.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType4.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType4.getText().toString(), "1");
				}
				else{

					//showToast("Uncheck 4");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType4.getText().toString(), "0");
				}
			}
		});
		mCheckBox5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mCheckBox5.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType5.getText().toString(), "1");
				}
				else{

					////showToast("Uncheck 5");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionType5.getText().toString(), "0");
				}
			}
		});

		//========================= type 1 ends here ========================================

		// check boxes clicks for type txt - tick

		mCheckBoxTicNText1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(mCheckBoxTicNText1.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText1.getText().toString(), "1");
				}
				else{

					////showToast("Uncheck 5");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText1.getText().toString(), "0");
				}
			}
		});

		mCheckBoxTicNText2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(mCheckBoxTicNText2.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText2.getText().toString(), "1");
				}
				else{

					////showToast("Uncheck 5");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText2.getText().toString(), "0");
				}
			}
		});

		mCheckBoxTicNText3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(mCheckBoxTicNText3.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText3.getText().toString(), "1");
				}
				else{

					////showToast("Uncheck 5");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText3.getText().toString(), "0");
				}
			}
		});

		mCheckBoxTicNText4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(mCheckBoxTicNText4.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText4.getText().toString(), "1");
				}
				else{

					////showToast("Uncheck 5");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText4.getText().toString(), "0");
				}
			}
		});

		mCheckBoxTicNText5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(mCheckBoxTicNText5.isChecked()){

					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText5.getText().toString(), "1");
				}
				else{

					////showToast("Uncheck 5");
					mquestionModelObj.get(mRecordCount).setmMap(mOptionTypeTicNText5.getText().toString(), "0");
				}
			}
		});

		//		mCheckBox1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		//			@Override
		//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		//				// TODO Auto-generated method stub
		//
		//				showToast("State changed");
		//				if(buttonView.isChecked()){
		//
		//					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType1.getText().toString());
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType1.getText().toString(), "1");
		//				}
		//				else{
		//
		//					//showToast("Uncheck 1");
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType1.getText().toString(), "0");
		//				}
		//			}
		//		});
		//
		//		mCheckBox2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		//			@Override
		//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		//				// TODO Auto-generated method stub
		//
		//				if(buttonView.isChecked()){
		//
		//					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType2.getText().toString());
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType2.getText().toString(), "1");
		//				}
		//				else{
		//
		//					//showToast("Uncheck 2");
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType2.getText().toString(), "0");
		//				}
		//			}
		//		});
		//
		//
		//		mCheckBox3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		//			@Override
		//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		//				// TODO Auto-generated method stub
		//
		//				if(buttonView.isChecked()){
		//
		//					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType3.getText().toString());
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType3.getText().toString(), "1");
		//				}
		//				else{
		//
		//					//showToast("Uncheck 3");
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType3.getText().toString(), "0");
		//				}
		//			}
		//		});
		//
		//		mCheckBox4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		//			@Override
		//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		//				// TODO Auto-generated method stub
		//
		//				if(buttonView.isChecked()){
		//
		//					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType4.getText().toString());
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType4.getText().toString(), "1");
		//				}
		//				else{
		//
		//					//showToast("Uncheck 4");
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType4.getText().toString(), "0");
		//				}
		//			}
		//		});
		//
		//		mCheckBox5.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		//			@Override
		//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		//				// TODO Auto-generated method stub
		//
		//				if(buttonView.isChecked()){
		//
		//					//mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType5.getText().toString(), "1");
		//				}
		//				else{
		//
		//					////showToast("Uncheck 5");
		//					mquestionModelObj.get(mRecordCount).setmMap(mOptionType5.getText().toString(), "0");
		//				}
		//			}
		//		});

		//		if(mRecordCount>=mquestionModelObj.size()-1){
		//
		//			
		//		}
		//		else{
		//			
		//			mSubmitButton.setVisibility(View.GONE);
		//		}

		mSubmitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mQuestionViewProgressBar.setVisibility(View.VISIBLE);
				callQuestionSubmitApi();

				//finish();

			}
		});

		mFinalPrevBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mImageAddView.setVisibility(View.GONE);
				mFinalButtonView.setVisibility(View.GONE);
				mButtonView.setVisibility(View.VISIBLE);
				setAnswer();
				showQuestion();

			}
		});


		//for adding image of Damages
		mDamageImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				isDamage = true;
				//isApproval = false;
				showPicturePickerDialog();
			}
		});

		// for add digital signature of Retailer
		mApprovalSignImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				isApproval = true;
				//isDamage = false;
				Intent intent = new Intent(mContext, DigitalSignature.class);
				startActivityForResult(intent, 20);

			}
		});
	}



	protected void callQuestionSubmitApi() {
		// TODO Auto-generated method stub

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("content-type", "application/json");

		JSONObject json = new JSONObject();		
		try {


			json.put("member_id", mMember_ID);
			json.put("auth_id", mMember_Auth_id);
			json.put("store_id", store_id);
			json.put("category_id",category_id);
			json.put("approved", "1");

			if(mApprovalId!=null){

				json.put("approval_id", mApprovalId);
			}

			if(isApproval){

				if(finalSignature!=null){
					json.put("image_approval_done", "1");
				}
				else{
					json.put("image_approval_done", "0");
				}
			}

			if(isDamage){

				if(mByteArr!=null){
					json.put("image_inspection_done", 1);
				}
				else{
					json.put("image_inspection_done", 0);
				}


			}


			JSONArray answer_data = new JSONArray();

			//size check


			for(int i=0;i<mquestionModelObj.size();i++){

				String ques_id = mquestionModelObj.get(i).getQuestion_id();
				String ques_type = mquestionModelObj.get(i).getQuestion_type();


				if(ques_type.equalsIgnoreCase("tic")){

					JSONObject obj = new JSONObject();
					obj.put("question_id", ques_id);
					obj.put("question_type",ques_type);


					Log.d("Ans","Ans " + "intic");

					JSONObject answers = new JSONObject();
					JSONArray answer = new JSONArray();

					ArrayList<String> keyNames = getKeys(mquestionModelObj.get(i).getmMap());

					if(keyNames.size()==0){

						continue;
					}

					for(int m=0; m<keyNames.size();m++){

						answer.put(m, keyNames.get(m));
					}

					answers.put("answer", answer);
					obj.put("answers", answers);
					answer_data.put(obj);
				}
				else if(ques_type.equalsIgnoreCase("tic-txt")){

					JSONObject obj = new JSONObject();
					obj.put("question_id", ques_id);
					obj.put("question_type",ques_type);

					JSONObject answers = new JSONObject();
					JSONArray answer = new JSONArray();

					if(mEditTickNTxt.length()!=0){
						answers.put("text", mEditTickNTxt.getText().toString());
					}
					else{
						answers.put("text", "");
					}

					ArrayList<String> keyNames = getKeys(mquestionModelObj.get(i).getmMap());

					if(keyNames.size()!=0){

						for(int m=0; m<keyNames.size();m++){

							answer.put(m, keyNames.get(m));
						}


					}
					answers.put("answer", answer);
					obj.put("answers", answers);
					answer_data.put(obj);
				}
				else if(ques_type.equalsIgnoreCase("txt")){

					JSONObject obj = new JSONObject();
					obj.put("question_id", ques_id);
					obj.put("question_type",ques_type);


					Log.d("Ans","Ans " + "intxt");

					JSONObject answers = new JSONObject();

					if(mEditTxt.length()!=0){
						answers.put("text", mEditTxt.getText().toString());
					}
					else{
						answers.put("text", mEditTxt.getText().toString());
					}

					obj.put("answers", answers);

					answer_data.put(obj);
				}


			}

			json.put("answer_data", answer_data);

			writeToFile(json.toString());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, QUESTION_GET_URL, json, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mQuestionViewProgressBar.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE FINAL" + response.toString());

				String api_response = response.toString();
				//parseResponse(api_response);


				try {

					JSONObject jsonObj = new JSONObject(api_response);
					mSuccess = jsonObj.getString("success");


					if(mSuccess.equalsIgnoreCase("true")){

						JSONObject data = jsonObj.getJSONObject("data");
						mApprovalId = data.getString("id");

						showSuccesDialog();

					}else{


						String errorCode = jsonObj.getString("code");
						mMessage = jsonObj.getString("message");
						showToast(mMessage);
						if(errorCode.equalsIgnoreCase("-116")){

							showAlertDialog();
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mQuestionViewProgressBar.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}

	protected ArrayList<String> getKeys(Map<String, String> getmMap) {
		// TODO Auto-generated method stub

		ArrayList<String> tmpArrName = new ArrayList<String>();
		ArrayList<String> tmpArrValue = new ArrayList<String>();

		Iterator iter = getmMap.entrySet().iterator();
		Map.Entry mEntry = null;

		while (iter.hasNext()) {

			mEntry = (Map.Entry) iter.next();
			Log.d("HASHMAP","HASHMAP "  + mEntry.getKey() + " " + mEntry.getValue());

			//tmpArrName.add(mEntry.getKey().toString());

			if(mEntry.getValue().toString().equalsIgnoreCase("1")){

				tmpArrName.add(mEntry.getKey().toString());
			}

		}

		return tmpArrName;
	}

	protected void setAnswer() {
		// TODO Auto-generated method stub

		//chk for current question text
		//		if(mquestionModelObj.get(mRecordCount).getTxtBoxMessage().length()==0){
		//			
		//			mEditTxt.setText("");
		//			mEditTickNTxt.setText("");
		//		}

		//		if(mEditTxt.getVisibility() == View.VISIBLE){
		//			
		//			if(mquestionModelObj.get(mRecordCount).getTxtBoxMessage().length()==0){
		//				
		//				mEditTxt.setText("");
		//				//mEditTickNTxt.setText("");
		//			}
		//		}
		//		
		//		if(mEditTickNTxt.getVisibility() == View.VISIBLE){
		//			
		//			if(mquestionModelObj.get(mRecordCount).getTxtBoxMessage().length()==0){
		//			
		//				mEditTickNTxt.setText("");
		//			}
		//			
		//		}


		ArrayList<String> tmpName = new ArrayList<String>();
		ArrayList<String> tmpValue = new ArrayList<String>();


		Map<String, String> mMap = mquestionModelObj.get(mRecordCount).getmMap();
		Log.d("SIZE ","SIZE " + mMap.size() + ":" + mRecordCount + " " + mMap.toString());

		Iterator iter = mMap.entrySet().iterator();
		Map.Entry mEntry = null;

		while (iter.hasNext()) {
			mEntry = (Map.Entry) iter.next();
			System.out.println(mEntry.getKey() + " : " + mEntry.getValue());

			Log.d("HASHMAP","HASHMAP "  + mEntry.getKey() + " " + mEntry.getValue());

			tmpName.add(mEntry.getKey().toString());
			tmpValue.add(mEntry.getValue().toString());
		}


		//		Set keys = mMap.keySet();

		for(int i=0;i<mquestionModelObj.get(mRecordCount).getOptions().size();i++){

			for(int j=0;j<tmpName.size();j++){

				String type = mquestionModelObj.get(mRecordCount).getQuestion_type();

				if(mquestionModelObj.get(mRecordCount).getOptions().get(i).equalsIgnoreCase(tmpName.get(j))){

					if(tmpValue.get(j).equalsIgnoreCase("1")){

						if(type.equalsIgnoreCase("tic")){

							//showToast("type-tic");

							if(i == 0){

								mCheckBox1.setChecked(true);
							}
							else if(i == 1){

								mCheckBox2.setChecked(true);
							}
							else if(i == 2){

								mCheckBox3.setChecked(true);
							}
							else if(i == 3){

								mCheckBox4.setChecked(true);
							}
							else if(i == 4){

								mCheckBox5.setChecked(true);
							}
						}

						else if(type.equalsIgnoreCase("tic-txt")){

							//showToast("type-tic-txt");

							if(i == 0){

								mCheckBoxTicNText1.setChecked(true);
							}
							else if(i == 1){

								mCheckBoxTicNText2.setChecked(true);
							}
							else if(i == 2){

								mCheckBoxTicNText3.setChecked(true);
							}
							else if(i == 3){

								mCheckBoxTicNText4.setChecked(true);
							}
							else if(i == 4){

								mCheckBoxTicNText5.setChecked(true);
							}
						}
					}
				}
			}

		}


	}

	protected void getAnswer() {
		// TODO Auto-generated method stub


		if(mQuestionType.equalsIgnoreCase("multi")){

			if(mCheckBox1.isChecked()){

				mquestionModelObj.get(mRecordCount).setAnswers(mOptionType1.getText().toString());
			}
			if(mCheckBox2.isChecked()){

				mquestionModelObj.get(mRecordCount).setAnswers(mOptionType2.getText().toString());
			}
			if(mCheckBox3.isChecked()){

				mquestionModelObj.get(mRecordCount).setAnswers(mOptionType3.getText().toString());
			}
			if(mCheckBox4.isChecked()){

				mquestionModelObj.get(mRecordCount).setAnswers(mOptionType4.getText().toString());
			}
			if(mCheckBox5.isChecked()){

				mquestionModelObj.get(mRecordCount).setAnswers(mOptionType5.getText().toString());
			}


		}
	}

	void callAllQuestionsApi() {
		// TODO Auto-generated method stub

		Log.d("URL ","URL " + QUESTION_GET_URL);

		mQuestionViewProgressBar.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("member_id", mMember_ID);
		headers.put("auth_id", mMember_Auth_id);

		Log.d("MEM_ID ","MEM_ID " +  mMember_ID + " " + mMember_Auth_id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, QUESTION_GET_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mQuestionViewProgressBar.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseResponse(api_response);
			}



		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mQuestionViewProgressBar.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}

	void parseResponse(String api_response) {
		// TODO Auto-generated method stub


		try {

			JSONObject json = new JSONObject(api_response);
			mSuccess = json.getString("success");

			if(mSuccess.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");
				for(int i=0;i<data.length();i++){

					questionModel = new InstallationQuestionModel();

					JSONObject questionDetails = data.getJSONObject(i);
					questionModel.setCategory_id(questionDetails.getString("category_id"));
					questionModel.setStore_id(questionDetails.getString("store_id"));
					questionModel.setQuestion(questionDetails.getString("question"));
					questionModel.setQuestion_id(questionDetails.getString("question_id"));
					questionModel.setQuestion_type(questionDetails.getString("question_type"));
					questionModel.setOption_type(questionDetails.getString("option_type"));

					JSONArray optionsArray = questionDetails.getJSONArray("options");

					for(int j=0;j<optionsArray.length();j++){

						questionModel.setOptions(optionsArray.getString(j));
						questionModel.setmMap(optionsArray.getString(j), "0");
					}

					if(questionDetails.has("answers")){

						JSONObject answers = questionDetails.getJSONObject("answers");
						if(questionDetails.getString("question_type").equalsIgnoreCase("tic-txt")){

							mEditTickNTxt.setText(answers.getString("text"));
						}
						else if(questionDetails.getString("question_type").equalsIgnoreCase("txt")){

							mEditTxt.setText(answers.getString("text"));
						}

						for(int indx=0;indx<answers.length();indx++){

							JSONArray answer = answers.getJSONArray("answer");

							for(int m=0; m<answer.length();m++){

								questionModel.setmMap(answer.getString(m), "1");
							}
						}
					}

					mquestionModelObj.add(questionModel);
				}

				for(int i=0;i<mquestionModelObj.size();i++){

					Log.d("SIZES","SIZES " + mquestionModelObj.get(i).getmMap().size());
				}

				setAnswer();
				showQuestion();

			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");


				if(errorCode.equalsIgnoreCase("-116")){

					showAlertDialog();
				}
				else{

					showToast(errorMessage);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showQuestion() {
		// TODO Auto-generated method stub

		//Toast.makeText(mContext, "" +  mRecordCount, 0).show();
		// for initial state of the question view --- 1st question

		mQuestionType = mquestionModelObj.get(mRecordCount).getQuestion_type();
		mOptionType = mquestionModelObj.get(mRecordCount).getOption_type();
		totalOptions = mquestionModelObj.get(mRecordCount).getOptions();



		if(mQuestionType.equalsIgnoreCase("tic")){

			// gone all other parent and child relatives layouts
			mParentText.setVisibility(View.GONE);
			mParentTicPlusTxt.setVisibility(View.GONE);
			mOptionTicNTextRel4.setVisibility(View.GONE);
			mOptionTicNTextRel5.setVisibility(View.GONE);

			mSingleChoice.setVisibility(View.VISIBLE);
			mQuesTypeCheck.setText(mquestionModelObj.get(mRecordCount).getQuestion());


			if(mOptionType.equalsIgnoreCase("multi")){

				//showToast("in multi");

				mOptionTypeCheckRel4.setVisibility(View.VISIBLE);
				mOptionTypeCheckRel5.setVisibility(View.VISIBLE);

				mOptionType1.setText(totalOptions.get(0));
				mOptionType2.setText(totalOptions.get(1));
				mOptionType3.setText(totalOptions.get(2));
				mOptionType4.setText(totalOptions.get(3));
				mOptionType5.setText(totalOptions.get(4));
			}
			else{

				//showToast("in single");

				mOptionTypeCheckRel4.setVisibility(View.GONE);
				mOptionTypeCheckRel5.setVisibility(View.GONE);

				mOptionType1.setText(totalOptions.get(0));
				mOptionType2.setText(totalOptions.get(1));
				mOptionType3.setText(totalOptions.get(2));

			}

		}
		else if(mQuestionType.equalsIgnoreCase("tic-txt")){

			// gone all other parent and child relatives layouts.
			mParentText.setVisibility(View.GONE);
			mSingleChoice.setVisibility(View.GONE);
			mOptionTypeCheckRel4.setVisibility(View.GONE);
			mOptionTypeCheckRel5.setVisibility(View.GONE);


			mParentTicPlusTxt.setVisibility(View.VISIBLE);
			mTickPlusText = mEditTickNTxt.getText().toString();

			mQuesTickNText.setText(mquestionModelObj.get(mRecordCount).getQuestion());

			if(mOptionType.equalsIgnoreCase("multi")){


				mOptionTicNTextRel4.setVisibility(View.VISIBLE);
				mOptionTicNTextRel5.setVisibility(View.VISIBLE);

				mOptionTypeTicNText1.setText(totalOptions.get(0));
				mOptionTypeTicNText2.setText(totalOptions.get(1));
				mOptionTypeTicNText3.setText(totalOptions.get(2));
				mOptionTypeTicNText4.setText(totalOptions.get(3));
				mOptionTypeTicNText5.setText(totalOptions.get(4));
			}
			else{

				mOptionTypeTicNText1.setText(totalOptions.get(0));
				mOptionTypeTicNText2.setText(totalOptions.get(1));
				mOptionTypeTicNText3.setText(totalOptions.get(2));
			}

		}
		else if(mQuestionType.equalsIgnoreCase("txt")){

			// Gone all other type of ques, which r visible
			mSingleChoice.setVisibility(View.GONE);
			mParentTicPlusTxt.setVisibility(View.GONE);
			mOptionTypeCheckRel4.setVisibility(View.GONE);
			mOptionTypeCheckRel5.setVisibility(View.GONE);
			mOptionTicNTextRel4.setVisibility(View.GONE);
			mOptionTicNTextRel5.setVisibility(View.GONE);

			mParentText.setVisibility(View.VISIBLE);
			mQuesText.setText(mquestionModelObj.get(mRecordCount).getQuestion());
			mEditTick = mEditTxt.getText().toString();

		}
	}

	void showToast(String msg){

		Toast.makeText(mContext, ""+ msg, Toast.LENGTH_SHORT).show();
	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutMember();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					//logout customer
					session.logoutMember();
					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();



		}
		return true;
	}

	private void writeToFile(String data) {

		try {
			File myFile = new File("/sdcard/questions.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();


		} 
		catch (Exception e) 
		{
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 20) {

			if(resultCode == RESULT_OK){

				int result=data.getIntExtra("result",0);
				finalSignature = data.getByteArrayExtra("SIGNATURE");

				if(finalSignature!=null){

					Bitmap background = BitmapFactory.decodeByteArray(finalSignature, 0,finalSignature.length);
					mApprovalSignImage.setImageBitmap(background);

					callApprovalImgApi();
				}
				else{

					showToast("Unable to process, Please try again later");
				}

			}
			if (resultCode == RESULT_CANCELED) {
				//Write your code if there's no result
			}
		}
		else{

			switch (requestCode) {


			case REQUEST_CODE_CUSTOMCAMERA:

				if(resultCode == Activity.RESULT_OK){

					try{
						Bitmap bitmap = null;

						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480);

						if(bitmap != null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							mByteArr = stream.toByteArray();
							cameraImageSave.deleteFromFile();
							cameraImageSave = null;
							bitmap = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDamageImage.setImageBitmap(bmp);

						callApprovalImgApi();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				break;

			case REQUEST_CODE_POSTGALLERY:
				if(resultCode == Activity.RESULT_OK) {
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(mContext, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQUEST_CODE_POSTCROPGALLERY);
				}

				break;

			case REQUEST_CODE_POSTCROPGALLERY:
				if(resultCode == Activity.RESULT_OK){
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";

					CameraImageSave cameraSaveImage = new CameraImageSave();

					String filePath = cameraSaveImage.getImagePath();


					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480);

						Toast.makeText(mContext, "" + bitmap, 0).show();
						if(bitmap!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							cameraSaveImage.deleteFromFile();
							mByteArr = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						Bitmap bmp = BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length);
						mDamageImage.setImageBitmap(bmp);

						callApprovalImgApi();
					}
					catch(Exception e){
						Log.e("Exception","Exception at Create broacast class");
						e.printStackTrace();
					}
				}
				break;


			default:
				break;
			}
		}


	}//onActivityResult


	protected void showPicturePickerDialog() {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage("Choose an option")
		.setCancelable(true)
		.setPositiveButton("Take Picture",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				takePictureFromCustomCamera();
			}
		})
		.setNegativeButton("Gallery",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				openGallery();
			}
		});


		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	protected void openGallery() {
		// TODO Auto-generated method stub

		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");


		startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY);

	}



	private void takePictureFromCustomCamera() {
		try {

			Intent intent = new Intent(mContext,CameraView.class);
			intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
			startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA);

		} catch (ActivityNotFoundException e) {
			Log.d("", "cannot take picture", e);
		}
	}

	//calling approval image api
	private void callApprovalImgApi() {
		// TODO Auto-generated method stub

		mQuestionViewProgressBar.setVisibility(View.VISIBLE);
		showToast("Please wait...");

		Log.d("URL ","URL " + APPROVAL_IMG_URL);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");

		JSONObject json=new JSONObject();
		try {

			json.put("store_id", store_id);
			json.put("member_id", mMember_ID);
			json.put("auth_id", mMember_Auth_id);
			json.put("category_id", category_id);
			json.put("filetype", FILE_TYPE);

			Log.d("ISBOOLEAN","ISBOOLEAN " + isDamage + " " + isApproval);
			if(isApproval){

				if(finalSignature!=null){

					String user_file=Base64.encodeToString(finalSignature, Base64.DEFAULT);
					json.put("approval_img", user_file);
					//json.put("image_approval_done", "1");
				}
				else{

					//json.put("image_approval_done", "0");
				}
			}

			if(isDamage){

				if(mByteArr!=null){

					String user_file=Base64.encodeToString(mByteArr, Base64.DEFAULT);
					json.put("inspection_img", user_file);
					//json.put("image_inspection_done", 1);
				}
				else{

					//json.put("image_inspection_done", 0);
				}


			}
			if(mApprovalId!=null){

				json.put("approval_id", mApprovalId);
			}


			writeToFile(json.toString());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, APPROVAL_IMG_URL, json, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mQuestionViewProgressBar.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				//parseResponse(api_response);


				try {

					JSONObject jsonObj = new JSONObject(api_response);
					mSuccess = jsonObj.getString("success");

					if(mSuccess.equalsIgnoreCase("true")){

						JSONObject data = jsonObj.getJSONObject("data");
						mApprovalId = data.getString("id");

					}else{

						String errorMessage = jsonObj.getString("message");
						String errorCode = jsonObj.getString("code");

						showToast(errorMessage);
						if(errorCode.equalsIgnoreCase(getResources().getString(R.string.invalid_auth_code))){

							showAlertDialog();
						}
						else{

							showToast(errorMessage);
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mQuestionViewProgressBar.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	void showSuccesDialog() {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage("Data submitted successfully.")
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				Intent intent = new Intent(mContext, MemberModuleSelection.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
