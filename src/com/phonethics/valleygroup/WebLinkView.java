package com.phonethics.valleygroup;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.phonethics.valleygroup.LiveStreaming.MyWebViewClient;

public class WebLinkView extends ActionBarActivity {

	android.support.v7.app.ActionBar mActionBar;
	Context mContext;
	WebView mWebView;
	ProgressBar mProgressBarWebView;
	String PHONETHICS_URL = "http://phonethics.in/digital-agency-contact-us/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_link_view);

		mContext = this;
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Phonethics");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));
		mProgressBarWebView = (ProgressBar) findViewById(R.id.mProgressBarWebView);
		mWebView = (WebView) findViewById(R.id.mWebView);
		
		
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setPluginState(PluginState.ON);
		mWebView.getSettings().setAllowFileAccess(true);
		mWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
		mWebView.setWebViewClient(new MyWebViewClient());
		mWebView.loadUrl(PHONETHICS_URL);

	}

	class MyWebViewClient extends WebViewClient {

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub

			mProgressBarWebView.setVisibility(View.GONE);
			super.onPageFinished(view, url);

		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub

			mProgressBarWebView.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);


		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			super.onReceivedError(view, errorCode, description, failingUrl);
			//showToast(description);
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	
}
