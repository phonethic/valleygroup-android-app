package com.phonethics.valleygroup;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class InTransitState extends ActionBarActivity implements LocationListener {

	TextView mLocationAddress;
	GoogleMap googleMap;
	SupportMapFragment fm;
	LocationManager locationManager;
	Location mLocation;
	Context mContext;
	String provider;
	NetworkInfo netInfo;
	ConnectivityManager cm; 
	String STORE_STATUS_URL;
	String mCustomer_ID;
	String mAuth_Id;
	HashMap<String, String> mCustomerDetails = new HashMap<String, String>();
	SessionManager session;
	String STORE_ID;
	String success;
	String mStoreNameRes;
	String mStoreIdRes;
	String mStoreLatRes;
	String mStoreLonRes;
	ProgressBar mProgressBarInTransitState;
	Button mMoreBtn;
	String mAddress;
	Typeface mTypeFace;
	ActionBar mActionBar;
	String mTracking_Url;
	String mTracking_Hashkey;
	String mTrackString_UserName;
	String mTracking_UserId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_in_transit_state);

		mContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("GPS Location");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));


		Bundle bundle=getIntent().getExtras();

		if(bundle!=null){

			STORE_ID = bundle.getString("STORE_ID");
		}

		cm 	= (ConnectivityManager)  getSystemService(Context.CONNECTIVITY_SERVICE);
		netInfo = cm.getActiveNetworkInfo();
		mLocationAddress = (TextView) findViewById(R.id.mLocationAddress);
		mProgressBarInTransitState = (ProgressBar) findViewById(R.id.mProgressBarInTransitState);
		mMoreBtn = (Button) findViewById(R.id.mMoreBtn);

		//store status url
		STORE_STATUS_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api)+getResources().getString(R.string.store_status);

		STORE_STATUS_URL = STORE_STATUS_URL + "?store_id=" + STORE_ID;

		mCustomerDetails = session.getUserDetails();
		mCustomer_ID = mCustomerDetails.get(SessionManager.CUSTOMER_ID);
		mAuth_Id = mCustomerDetails.get(SessionManager.AUTH_ID);

		mTypeFace = Typeface.createFromAsset(getAssets(),"fonts/Arial_Bold.ttf");
		mMoreBtn.setTypeface(mTypeFace);
		// Getting Google Play availability status
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

		// Showing status
		if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
			dialog.show();

		}
		else{

			//chk for Location Service on or not
			LocationManager lm = null;
			boolean gps_enabled = false,network_enabled = false;
			if(lm==null)
				lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
			try{
				gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
			}catch(Exception ex){}
			try{
				network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			}catch(Exception ex){}

			if(!gps_enabled && !network_enabled){
				Builder dialog = new AlertDialog.Builder(mContext);
				dialog.setMessage("Location service not enabled. Do you want to enable it.");
				dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface paramDialogInterface, int paramInt) {
						// TODO Auto-generated method stub
						Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
						mContext.startActivity(myIntent);
						//get gps
					}
				});
				dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface paramDialogInterface, int paramInt) {
						// TODO Auto-generated method stub

					}
				});
				dialog.show();

			}
			else{
				
				//showToast("Already enabled");
				
			}

			// commented bcoz of GPS device XML PARSING
			callStoreStatusApi();

			// Call XML PARSING for GPS device response
			//callGpsDeviceApi();
			//			MyAsyncTask m = new MyAsyncTask();
			//			m.execute();

			fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mTransitStatusMap);

			// Getting GoogleMap object from the fragment
			googleMap = fm.getMap();
			// Enabling MyLocation Layer of Google Map
			googleMap.setMyLocationEnabled(true);
			// Getting LocationManager object from System Service LOCATION_SERVICE
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


			//			if (netInfo !=null && netInfo.isConnected()) {
			//
			//
			//				if(isGpsEnable()){
			//
			//
			//					provider=LocationManager.GPS_PROVIDER;
			//					mLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			//
			//					if(mLocation==null){
			//						//onLocationChanged(location);
			//
			//						provider=LocationManager.NETWORK_PROVIDER;
			//						mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			//
			//						Log.d("Location","Location " + mLocation + " " + provider);
			//
			//						if(mLocation!=null){
			//
			//							onLocationChanged(mLocation);
			//						}
			//						else{
			//
			//							//Toast.makeText(getActivity(), "Unable to fetch your current location at the moment. Please try again later.", Toast.LENGTH_LONG).show();
			//							showToast("Unable to fetch your current location at the moment. Please try again later.");
			//						}
			//
			//					}else {
			//						onLocationChanged(mLocation);
			//					}
			//
			//				}
			//				else{
			//
			//					showGpsAlert();
			//				}
			//
			//			}
			//			else{
			//
			//				showToast("No internet connection"); 
			//			}
		}

		mMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(mContext, MoreOptionScreen.class);
				intent.putExtra("STORE_ID",STORE_ID);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			//logout customer
			//session.logoutCustomer();
			//			Intent intent = new Intent(mContext, LoginGateway.class);
			//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//			startActivity(intent);
			//			finish();

			//			Intent intent = new Intent(mContext, LoginGateway.class);
			//			ComponentName cn = intent.getComponent();
			//			Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
			//			mContext.startActivity(mainIntent);
			//			finish();
			//			overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					EventTracker.logEvent("Customer_Logout_Btn_Pressed", false);
					session.logoutCustomer();
					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);


				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		}
		return true;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		// Creating a LatLng object for the current location
		//		double latitude = Double.parseDouble(mStoreLatRes);
		//		double longitude = Double.parseDouble(mStoreLonRes);
		//
		//		LatLng latLng = new LatLng(latitude,longitude);
		//
		//		// Showing the current location in Google Map
		//		googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
		//
		//		// Zoom in the Google Map
		//		googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public void showToast(String message){

		Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
	}

	public boolean isGpsEnable(){
		boolean isGpsOn = false;
		try{
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return isGpsOn;
	}

	// Get Location Manager and check for GPS & Network location services

	public void showGpsAlert(){

		// Build the alert dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle("Location Services Not Active");
		builder.setMessage("Please enable Location Services and GPS Satellites for better Results");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				// Show location settings when the user acknowledges the alert dialog
				//					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				//					startActivityForResult(intent, 10);
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//if user doesnt turn on location services put location found as false
				//					try
				//					{
				//
				//						dialog.dismiss();
				//					}catch(Exception ex)
				//					{
				//						ex.printStackTrace();
				//					}
			}
		});
		Dialog alertDialog = builder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();


	}

	private void callStoreStatusApi() {
		// TODO Auto-generated method stub

		mProgressBarInTransitState.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("customer_id", mCustomer_ID);
		headers.put("auth_id", mAuth_Id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, STORE_STATUS_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				//mProgressBarInTransitState.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseResponse(api_response);
			}




		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mProgressBarInTransitState.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}

	private void parseResponse(String api_response) {
		// TODO Auto-generated method stub

		try {

			JSONObject json = new JSONObject(api_response);
			success = json.getString("success");

			if(success.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");
				for(int i=0; i<data.length();i++){

					JSONObject storeDetails = data.getJSONObject(i);

					mStoreIdRes = storeDetails.getString("store_id");
					mStoreNameRes = storeDetails.getString("store_name");
					//					mStoreLatRes = storeDetails.getString("latitude");
					//					mStoreLonRes = storeDetails.getString("longitude");
					mTracking_Url = storeDetails.getString("tracking_url");
					mTracking_Hashkey = storeDetails.getString("tracking_hashkey");
					mTrackString_UserName = storeDetails.getString("tracking_username");
					mTracking_UserId = storeDetails.getString("tracking_userid");

					//					Log.d("LAT ","LAT " + mStoreLatRes + " " + mStoreLonRes);
					//
					//					googleMap.addMarker(new MarkerOptions()
					//					.position(new LatLng(Double.parseDouble(mStoreLatRes),Double.parseDouble(mStoreLonRes)))
					//					.title("Current Location"));
					//
					//					// Showing the current location in Google Map
					//					googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(mStoreLatRes),Double.parseDouble(mStoreLonRes))));
					//
					//					// Zoom in the Google Map
					//					googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
					//
					//					// getting current address
					//
					//					double current_latitude = Double.parseDouble(mStoreLatRes);
					//					double current_longitude = Double.parseDouble(mStoreLonRes);
					//
					//					String current_address = "";
					//					Geocoder geoCoder = new Geocoder(
					//							getBaseContext(), Locale.getDefault());
					//					try {
					//						List<Address> addresses = geoCoder.getFromLocation(
					//								current_latitude, 
					//								current_longitude, 1);
					//
					//						if (addresses.size() > 0) {
					//							for (int index = 0; 
					//									index < addresses.get(0).getMaxAddressLineIndex(); index++)
					//								current_address += addresses.get(0).getAddressLine(index) + " ";
					//						}
					//					}catch (IOException e) {        
					//						e.printStackTrace();
					//					}   
					//
					//					//showToast(current_address);
					//					mLocationAddress.setText(current_address);

					MyAsyncTask m = new MyAsyncTask();
					m.execute();

				}


			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");
				

				if(errorCode.equalsIgnoreCase(getResources().getString(R.string.invalid_auth_code))){

					showAlertDialog();
				}
				else{
					
					showToast(errorMessage);
				}
			}



		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutCustomer();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);


				//								Intent intet = new Intent(mContext,LoginGateway.class);
				//								startActivity(intet);

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void callGpsDeviceApi(){

		StringBuilder builder=new StringBuilder();

		try {

			URL url = new URL(mTracking_Url);
			XmlPullParserFactory factory=XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp=factory.newPullParser();

			xpp.setInput(getInputStream(url), "UTF_8");

			int eventType=xpp.getEventType();
			while(eventType!=XmlPullParser.END_DOCUMENT){

				String tagName = xpp.getName();

				switch(eventType){

				case XmlPullParser.START_DOCUMENT:

					break;

				case XmlPullParser.START_TAG :

					if(tagName.equalsIgnoreCase("basic_tracking")){

						mStoreLatRes = xpp.getAttributeValue(null, "latitude");
						mStoreLonRes = xpp.getAttributeValue(null, "longitude");
						mAddress = xpp.getAttributeValue(null, "location");
					}

					break;
				}

				eventType = xpp.next();
			}



		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public InputStream getInputStream(URL url) {
		try {
			return url.openConnection().getInputStream();
		} catch (IOException e) {
			return null;
		}
	}

	class MyAsyncTask extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub


			callGpsDeviceApi();
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub

			super.onPostExecute(result);

			mProgressBarInTransitState.setVisibility(View.GONE);

			Log.d("XMLPARSING","XMLPARSING " + mStoreLatRes + " " + mStoreLonRes);
			//showToast("HI"  + mStoreLatRes);

			googleMap.addMarker(new MarkerOptions()
			.position(new LatLng(Double.parseDouble(mStoreLatRes),Double.parseDouble(mStoreLonRes)))
			.title("Shipment Location"));

			// Showing the current location in Google Map
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(mStoreLatRes),Double.parseDouble(mStoreLonRes))));

			// Zoom in the Google Map
			googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));

			mLocationAddress.setText(mAddress);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}


	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}
}
