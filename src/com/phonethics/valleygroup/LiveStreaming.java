package com.phonethics.valleygroup;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class LiveStreaming extends ActionBarActivity {

	Context mContext;
	VideoView mVideoView;
	String STORE_STATUS_URL;
	String STORE_ID;
	ProgressBar mProgressBarLiveStreaming;
	String mCustomer_ID;
	String mAuth_Id;
	HashMap<String, String> mCustomerDetails = new HashMap<String, String>();
	SessionManager session;
	String success;
	String Video_Url;
	Button mMoreBtn;
	WebView mWebView;
	Typeface mTypeFace;
	ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_live_streaming);

		mContext = this;
		session = new SessionManager(mContext);
		mActionBar = getSupportActionBar();
		mActionBar.setTitle("Live Video");
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getResources().getString(R.color.newRed))));

		//mVideoView = (VideoView) findViewById(R.id.mVideoView);
		mProgressBarLiveStreaming = (ProgressBar) findViewById(R.id.mProgressBarLiveStreaming);
		mMoreBtn = (Button) findViewById(R.id.mMoreBtn);
		mWebView = (WebView) findViewById(R.id.mWebView);
		mTypeFace = Typeface.createFromAsset(getAssets(),"fonts/Arial_Bold.ttf");
		mMoreBtn.setTypeface(mTypeFace);

		Bundle bundle=getIntent().getExtras();

		if(bundle!=null){

			STORE_ID = bundle.getString("STORE_ID");
		}


		//store status url
		STORE_STATUS_URL = getResources().getString(R.string.base_url) + getResources().getString(R.string.active_api) 
				+getResources().getString(R.string.store_api)+getResources().getString(R.string.store_status);

		STORE_STATUS_URL = STORE_STATUS_URL + "?store_id=" + STORE_ID;

		mCustomerDetails = session.getUserDetails();
		mCustomer_ID = mCustomerDetails.get(SessionManager.CUSTOMER_ID);
		mAuth_Id = mCustomerDetails.get(SessionManager.AUTH_ID);

		boolean installed  =   appInstalledOrNot("org.mozilla.firefox");  
		if(installed) {
			//This intent will help you to launch if the package is already installed
			//			Intent LaunchIntent = getPackageManager()
			//					.getLaunchIntentForPackage("com.Ch.Example.pack");
			//			startActivity(LaunchIntent);
			callLiveStreamVideoApi();
			//			showToast("App already installed on your phone");        
		}
		else {

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage("Mozilla Firefox is required to see live status. Do you want to install it.")
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse("https://play.google.com/store/apps/details?id=org.mozilla.firefox"));
					startActivity(i);
				}
			})
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			//showToast("App is not installed on your phone");
		}



		mMoreBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(mContext, MoreOptionScreen.class);
				intent.putExtra("STORE_ID",STORE_ID);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
	}

	private boolean appInstalledOrNot(String uri) {
		PackageManager pm = getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		}
		catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed ;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		MenuItem extra=menu.add("Logout").setTitle("Logout");
		//Icon(R.drawable.switch_view);
		MenuItemCompat.setShowAsAction(extra,MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			//showToast("Logout pressed");

			//logout customer
			//			session.logoutCustomer();
			//
			//			Intent intent = new Intent(mContext, LoginGateway.class);
			//			ComponentName cn = intent.getComponent();
			//			Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
			//			mContext.startActivity(mainIntent);
			//			finish();
			//			overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mContext);

			// set title
			alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

			// set dialog message
			alertDialogBuilder
			.setMessage(getResources().getString(R.string.logout_confirmation_message))
			.setCancelable(true)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity

					EventTracker.logEvent("Customer_Logout_Btn_Pressed", false);
					session.logoutCustomer();
					Intent intent = new Intent(mContext, LoginGateway.class);
					ComponentName cn = intent.getComponent();
					Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
					mContext.startActivity(mainIntent);
					finish();
					overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);

				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
		}
		return true;
	}

	void callLiveStreamVideoApi() {
		// TODO Auto-generated method stub

		mProgressBarLiveStreaming.setVisibility(View.VISIBLE);

		final HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("X-API-KEY", "vgiapp");
		headers.put("customer_id", mCustomer_ID);
		headers.put("auth_id", mAuth_Id);

		RequestQueue queue = Volley.newRequestQueue(mContext);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, STORE_STATUS_URL, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub

				mProgressBarLiveStreaming.setVisibility(View.GONE);
				Log.d("RESPONSE ","RESPONSE " + response.toString());

				String api_response = response.toString();
				parseResponse(api_response);
			}


		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

				mProgressBarLiveStreaming.setVisibility(View.GONE);

				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		})
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError{

				// TODO Auto-generated method stub
				return headers;
			}
		};

		queue.add(jsObjRequest);

	}


	private void parseResponse(String api_response) {
		// TODO Auto-generated method stub


		try {

			JSONObject json = new JSONObject(api_response);
			success = json.getString("success");

			if(success.equalsIgnoreCase("true")){

				JSONArray data = json.getJSONArray("data");
				for(int i=0; i<data.length();i++){

					JSONObject storeStatus = data.getJSONObject(i);
					Video_Url = storeStatus.getString("video_url");
				}

				//				Uri uri = Uri.parse("http://democv10.cpplusddns.com/asp/video.cgi?profile=4&resolution=1280x720&random=0.4895150156226009"); //Declare your url here.
				//
				//				VideoView mVideoView  = (VideoView)findViewById(R.id.mVideoView);
				//				mVideoView.setMediaController(new MediaController(this));       
				//				mVideoView.setVideoURI(uri);
				//				mVideoView.requestFocus();
				//				mVideoView.start();
				//				
				//				mVideoView.setOnErrorListener(new OnErrorListener() {
				//					
				//					@Override
				//					public boolean onError(MediaPlayer mp, int what, int extra) {
				//						// TODO Auto-generated method stub
				//						
				//						
				//						return true;
				//					}
				//				});

				//				mWebView.getSettings().setJavaScriptEnabled(true);
				//				//web.getSettings().setPluginsEnabled(true);
				//				mWebView.getSettings().setPluginState(PluginState.ON);
				//				mWebView.getSettings().setAllowFileAccess(true);
				//				mWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
				//				mWebView.setWebViewClient(new MyWebViewClient());
				//				mWebView.loadUrl("http://democv10.cpplusddns.com/asp/video.cgi?profile=4&resolution=1280x720&random=0.4895150156226009");

				//String url = "http://democv10.cpplusddns.com/asp/video.cgi?profile=4&resolution=1280x720&random=0.4895150156226009";

				try {

					//					Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("org.mozilla.firefox");
					//					LaunchIntent.setData(Uri.parse(Video_Url));
					//					startActivity(LaunchIntent);

					//show dialog for mozilla firefox

					//					final Dialog dialog = new Dialog(mContext);
					//					dialog.setContentView(R.layout.browserpicker);
					//					dialog.setTitle("Title...");
					//
					//					// set the custom dialog components - text, image and button
					//					TextView text = (TextView) dialog.findViewById(R.id.mBrowserName);
					//					//text.setText("Android custom dialog example!");
					//					ImageView image = (ImageView) dialog.findViewById(R.id.mBrowserIcon);
					//					//image.setImageResource(R.drawable.ic_launcher);
					//
					//					RelativeLayout dialogButton = (RelativeLayout) dialog.findViewById(R.id.dialogView);
					//					// if button is clicked, close the custom dialog
					//					dialogButton.setOnClickListener(new OnClickListener() {
					//						@Override
					//						public void onClick(View v) {
					//
					//
					//							showToast("clicked on mozilla");
					//						}
					//					});
					//
					//					dialog.show();

					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(Video_Url));
					startActivity(i);

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}


			}
			else{

				String errorMessage = json.getString("message");
				String errorCode = json.getString("code");


				if(errorCode.equalsIgnoreCase(getResources().getString(R.string.invalid_auth_code))){

					showAlertDialog();
				}
				else{

					showToast(errorMessage);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	public void showToast(String message){

		Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mContext);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logoutMessage))
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				session.logoutCustomer();
				Intent intent = new Intent(mContext, LoginGateway.class);
				ComponentName cn = intent.getComponent();
				Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
				mContext.startActivity(mainIntent);
				finish();
				overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_up);
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	class MyWebViewClient extends WebViewClient {

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub

			mProgressBarLiveStreaming.setVisibility(View.GONE);
			super.onPageFinished(view, url);

		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub

			mProgressBarLiveStreaming.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);


		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			super.onReceivedError(view, errorCode, description, failingUrl);
			showToast(description);
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EventTracker.endLocalyticsSession(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(mContext);
		//EventTracker.logEvent("Valley Group app started", false);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(mContext);
	}
}
